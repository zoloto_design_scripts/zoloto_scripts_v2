﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');

main();

function main() {
    var doc = app.activeDocument;
    var layer = doc.activeLayer;
    var spl = layer.splineItems;

    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, spl.length);

    var vars = [];
    while (vars.length < 100)
        vars.push(String(vars.length + 1));
    layer.insertLabel('VarsNames', vars.toSource());

    alert('Done');
}
