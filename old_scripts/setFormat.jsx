﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');

main();

function main() {
    var doc = app.activeDocument;
    var layer = doc.activeLayer;
    var layerName = layer.name;

    var prevVariableFormat = [];
    var lblVariableFormat = layer.extractLabel('VarsNames');
    if (lblVariableFormat && lblVariableFormat !== '')
        prevVariableFormat = eval(lblVariableFormat);
    var variableFormat = askFormat(layerName, prevVariableFormat);

    if (!variableFormat) {
        alert('Отменено.');
    }
    else {
        layer.insertLabel('VarsNames', variableFormat.toSource());
        alert('Переменные для типа ' + layerName + ' настроены.');
    }
}

function askFormat(layerName, prevVariableFormat) {
    var variableFormat = prevVariableFormat;
    var messageRemove = 'Выберите переменную для удаления:';
    var messageAdd = 'Введите имя переменной для добавления:';

    var w = new Window('dialog', 'Формат переменных типа ' + layerName);
    w.pnlVars = w.add('panel', undefined, 'Переменные');
    w.pnlVars.alignChildren = 'left';
    // statictext для надписи с числом переменных
    w.pnlVars.lblR = w.pnlVars.add('statictext', undefined, messageRemove);
    // listbox с именами переменных 
    w.pnlVars.list = w.pnlVars.add('listbox', { x: 0, y: 0, width: 300, height: 400 }, undefined, { items: prevVariableFormat });
    w.pnlVars.list.minimumSize = { width: 80, height: 120 };
    w.pnlVars.grRemove = w.pnlVars.add('group');
    w.pnlVars.grRemove.btn = w.pnlVars.grRemove.add('button', undefined, 'Удалить');
    w.pnlVars.grRemove.btn.onClick = function () {
        w.pnlVars.list.remove(w.pnlVars.list.selection);
    }
    w.pnlVars.lblA = w.pnlVars.add('statictext', undefined, messageAdd);
    w.pnlVars.grAdd = w.pnlVars.add('group');
    w.pnlVars.grAdd.lbl = w.pnlVars.grAdd.add('statictext', undefined, 'Имя: ');
    w.pnlVars.grAdd.edt = w.pnlVars.grAdd.add('edittext');
    w.pnlVars.grAdd.edt.minimumSize = { width: 100, height: 10 };
    w.pnlVars.grAdd.btn = w.pnlVars.grAdd.add('button', undefined, 'Добавить');
    w.pnlVars.grAdd.btn.onClick = function () {
        variableFormat = [];
        var listItems = w.pnlVars.list.items;
        for (var v = 0; v < listItems.length; ++v)
            variableFormat.push(listItems[v].text);

        var addItem = w.pnlVars.grAdd.edt.text;
        if (indexOf(variableFormat, addItem) !== -1)
            alert('Имена переменных не должны повторяться!');
        else
            w.pnlVars.list.add('item', addItem);
    }

    w.grBtns = w.add('group');
    w.grBtns.alignment = 'right';
    w.grBtns.btnCancel = w.grBtns.add('button', undefined, 'Отмена');
    w.grBtns.btnCancel.onClick = function () {
        variableFormat = undefined;
        w.close();
    }
    w.grBtns.btnOK = w.grBtns.add('button', undefined, 'ОК');
    w.grBtns.btnOK.onClick = function () {
        variableFormat = [];
        var listItems = w.pnlVars.list.items;
        for (var v = 0; v < listItems.length; ++v)
            variableFormat.push(listItems[v].text);
        w.close();
    }
    w.grBtns.btnOK.active = true;
    w.show();

    return variableFormat;
}
