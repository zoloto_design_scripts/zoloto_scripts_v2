﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech//tech//tech//tech/

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/enumerate.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/geometry.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/captions.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/styles.jsx');

var numberListFilePath = '/numbers.csv';

main();

function main() {
    var doc = app.activeDocument;
    var dir = doc.filePath;
    var layer = doc.activeLayer;
    var origName = layer.name;
    var markers = layer.splineItems;

    //if (!enumerateCarefull(doc))
    //    return;

    var styles = getNumberStyles(doc.characterStyles, doc.paragraphStyles, doc.objectStyles, doc.colors, origName);
    if (!styles)
        return;

    var numbersLayer = createCaptionLayer(doc.layers, origName, layer.layerColor, 'numbers');

    generateCaptions(markers, numbersLayer, styles, 'Number');

    doc.activeLayer = layer;
    // doc.save();
    alert('Выполнено.\nПронумеровано ' + markers.length + ' объектов.');

    var numberListFile = new File(dir + '/' + origName + '.csv');
    var numberLabelArr = collectNumberLabels(layer);
    writeFile(numberListFile, numberLabelArr.join('\n'), 'UTF-8');
    alert('Список номеров\nсохранён в файл ' + numberListFile.fullName);
}

function createCaption(marker, styles, captionsLayer) {
    var numberLabel = marker.parentPage.textFrames.add();
    numberLabel.move(captionsLayer);
    numberLabel.insertLabel('MarkerId', marker.id.toString());

    numberLabel.applyObjectStyle(styles.object);
    numberLabel.parentStory.texts[0].applyCharacterStyle(styles.character);

    var mostLeftPoint = takeMostLeft(marker);
    numberLabel.geometricBounds = [mostLeftPoint[1], mostLeftPoint[0] - 2,
    mostLeftPoint[1] + 4, mostLeftPoint[0] + 2];
    return numberLabel;
}

function modifyCaption(caption, marker) {
    var oldStyleNumber = marker.extractLabel('number');
    var lblNumber = (oldStyleNumber === ''
        ? marker.extractLabel('Number')
        : oldStyleNumber);
    if (caption.contents !== lblNumber) {
        caption.contents = lblNumber;
        caption.fit(FitOptions.FRAME_TO_CONTENT);
    }
}

function collectNumberLabels(layer) {
    var splineItems = layer.splineItems;
    var labelsArr = [];
    for (var i = 0; i < splineItems.length; ++i)
        labelsArr.push(splineItems[i].extractLabel('Number'));

    function labelCompFabric(nPos) { // label2 - label1
        var comp = function (label1, label2) {
            var label1Parts = label1.split('/');
            var label2Parts = label2.split('/');
            return label1Parts[nPos] - label2Parts[nPos];
        }
        return comp;
    }
    if (labelsArr.length > 0) {
        var p = labelsArr[0].split('/');
        var nPos = 0;
        while (Number(p[nPos]) !== Number(p[nPos])) {
            ++nPos;
            if (nPos === p.length) {
                nPos = -1;
                break;
            }
        }
    }
    if (nPos !== -1)
        labelsArr.sort(labelCompFabric(nPos));
    else
        labelsArr.sort();

    var lC = labelsArr.length;
    labelsArr.push('-------------\n');
    labelsArr.push('всего ' + lC + 'шт.');
    return labelsArr;
}
