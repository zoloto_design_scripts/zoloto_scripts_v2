﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');

main();

function main() {
    var doc = app.activeDocument;
    var layer = doc.activeLayer;
    var spl = layer.splineItems;

    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, spl.length);

    /*
    for (var m = 0; m < spl.length; ++m) {
        var S = spl[m];
        var vars = S.label.split('\r');
        for (var v = 0; v < vars.length; ++v) {
            var V = vars[v];
            if (V[V.length - 1] != ';')
                vars[v] = V.concat(';');
        }
        
        while (vars.length < 100)
            vars.push(';');
        S.label = vars.join('\r');
        progress.value++;
    }
    */


    var arrVarsNames = [];
    var useVarsNames = false;
    var lblVarsNames = layer.extractLabel('VarsNames');
    if (lblVarsNames && lblVarsNames !== '') {
        arrVarsNames = eval(lblVarsNames);
        if (arrVarsNames.length !== undefined)
            useVarsNames = true;
        else
            arrVarsNames = [];
    }

    var extendedVarNames = (useVarsNames ? arrVarsNames : []);
    while (extendedVarNames[extendedVarNames.length - 1] === 'undefined'
        || extendedVarNames[extendedVarNames.length - 1] === '\"undefined\"'
        || extendedVarNames[extendedVarNames.length - 1] === '\"\"undefined\"\"')
        extendedVarNames.pop();
    while (extendedVarNames.length < 100)
        extendedVarNames.push(String(extendedVarNames.length + 1));
    layer.insertLabel('VarsNames', extendedVarNames.toSource());

    alert('Done');
}
