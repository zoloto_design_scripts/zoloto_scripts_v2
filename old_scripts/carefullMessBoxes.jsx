﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech//tech//tech//tech/

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/enumerate.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/geometry.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/captions.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/styles.jsx');

main();

function main() {
    var doc = app.activeDocument;
    var layer = doc.activeLayer;
    var origName = layer.name;
    var markers = layer.splineItems;

    if (!enumerateCarefull(doc))
        return;

    var styles = getMessStyles(doc.paragraphStyles, doc.objectStyles, origName);
    if (!styles)
        return;

    var messLayer = createCaptionLayer(doc.layers, origName, layer.layerColor, 'mess');

    generateCaptions(markers, messLayer, styles, 'Mess');

    doc.activeLayer = layer;
    // doc.save();
    alert('Выполнено.\nПронумеровано ' + markers.length + ' объектов.');
}

function createCaption(marker, styles, captionsLayer) {
    var messCaption = marker.parentPage.textFrames.add();
    messCaption.move(captionsLayer);
    messCaption.insertLabel('MarkerId', marker.id.toString());

    messCaption.applyObjectStyle(styles.object);
    messCaption.parentStory.texts[0].applyParagraphStyle(styles.paragraph);

    var mostLeftPoint = takeMostLeft(marker);
    messCaption.geometricBounds = [mostLeftPoint[1], mostLeftPoint[0] - 2,
    mostLeftPoint[1] + 4, mostLeftPoint[0] + 2];

    /*var line = marker.parentPage.graphicLines.add({layer: captionsLayer});
    var markerRightPoint = takeMostRight(marker);
    var captionLeftTop = [messCaption.geometricBounds[1], messCaption.geometricBounds[0]];
    function getBoundingBox(p1, p2) {
        var lineLeftTop     = [(p1[0] < p2[0] ? p1[0] : p2[0]), (p1[1] < p2[1] ? p1[1] : p2[1])];
        var lineRightBottom = [(p1[0] > p2[0] ? p1[0] : p2[0]), (p1[1] > p2[1] ? p1[1] : p2[1])];
        return [lineLeftTop[1], lineLeftTop[0], lineRightBottom[1], lineRightBottom[0]];
    }
    line.geometricBounds = getBoundingBox(markerRightPoint, captionLeftTop);*/
    return messCaption;
}

function modifyCaption(caption, marker) {
    var mess = marker.extractLabel('Number')
        + '\r\r' + marker.label;
    if (caption.contents !== mess) {
        caption.contents = mess;
        caption.fit(FitOptions.FRAME_TO_CONTENT);
    }
}