﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech /

    $.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/checkMess.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/dataManipulations.jsx');

var data = [];

main();

function main() {
    var doc = app.activeDocument;
    var layer = doc.activeLayer;
    var origName = layer.name;

    var arrVarsNames = [];
    var useVarsNames = false;
    var lblVarsNames = layer.extractLabel('VarsNames');
    if (lblVarsNames && lblVarsNames !== '') {
        arrVarsNames = eval(lblVarsNames);
        if (arrVarsNames.length !== undefined)
            useVarsNames = true;
        else
            arrVarsNames = [];
    }

    // если переменных нет, сообщать "задайте формат переменных"
    if (!useVarsNames || arrVarsNames.length < 1) {
        alert('Для слоя ' + origName + ' не найдены имена переменных.\n' +
            'Задайте их, используя скрипт setFormat');
        return;
    }

    // отображать окошко, в котором каждой из 
    // переменных можно задать значение на данном маркере
    var selection = selectedSplines(doc);
    if (selection.length < 1)
        return;
    var oldSettedValues = aggregate(selection);
    var numbersOfSelected = getNumbers(selection);
    var varsValues = askValues(origName, numbersOfSelected, arrVarsNames, oldSettedValues);

    if (!varsValues) {
        alert('Отменено.');
        return;
    }

    // пока храним переменные в marker.label для совместимости со всем остальным
    resetValues(selection, varsValues);
}

function selectedSplines(doc) {
    var buf = [];
    var selection = doc.selection;
    for (var s = 0; s < selection.length; ++s) {
        var selected = selection[s];
        var type = selected.reflect.name;
        if (type === 'GraphicLine' || type === 'Oval'
            || type === 'Polygon' || type === 'Rectangle')
            buf.push(selected);
    }
    return buf;
}

function getNumbers(selection) {
    var numbers = [];
    for (var s = 0; s < selection.length; ++s)
        numbers.push(selection[s].extractLabel('Number'));
    return numbers;
}

function aggregate(selection) {
    var oldSettedValues = [];
    collectData(selection);
    for (var m = 0; m < data.length; ++m) {
        // сравниваем поэлементно массивы oldSettedValues и data[m];
        // для этого добиваем oldSettedValues '' до нужной длины
        while (oldSettedValues.length < data[m].length)
            oldSettedValues.push('');
        for (var v = 0; v < data[m].length; ++v) {
            // значение либо есть и не совпадает, тогда "[несколько значений]"
            if (oldSettedValues[v] !== data[m][v]) {
                if (oldSettedValues[v] === '') // первичная установка
                    oldSettedValues[v] = data[m][v];
                else
                    oldSettedValues[v] = '[несколько значений]';
            }
            // либо есть и совпадает, тогда ничего не делаем
        }
    }
    return oldSettedValues;
}

function askValues(layerName, nMarkers, arrVarsNames, oldSettedValues) {
    var varsValues = [];
    var title = 'Задать переменные';
    var msg = (nMarkers.length > 1
        ? 'Переменные ' + nMarkers.length + ' маркеров типа ' + layerName
        : 'Переменные маркера с номером ' + nMarkers[0] + ' типа ' + layerName);

    var w = new Window('dialog', title);
    w.lbl = w.add('statictext', undefined, msg);
    w.alignChildren = 'left';
    w.pnlVars = w.add('panel', undefined, 'Переменные');   // {x: 0, y: 0, width: 300, height: 400}
    w.pnlVars.alignChildren = 'left';
    for (var v = 0; v < arrVarsNames.length; ++v) {
        var grpName = 'grp' + v;

        w.pnlVars[grpName] = w.pnlVars.add('group');
        w.pnlVars[grpName].lbl = w.pnlVars[grpName].add('statictext', [undefined, undefined, 120, 20], arrVarsNames[v] + ': ');
        w.pnlVars[grpName].edt = w.pnlVars[grpName].add('edittext', [0, 0, 480, 20]);
        w.pnlVars[grpName].edt.text = (v < oldSettedValues.length ? oldSettedValues[v] : '');
    }

    w.grBtns = w.add('group');
    w.grBtns.alignment = 'left';
    w.grBtns.btnCancel = w.grBtns.add('button', undefined, 'Отмена');
    w.grBtns.btnCancel.onClick = function () {
        varsValues = undefined;
        w.close();
    }
    w.grBtns.btnOK = w.grBtns.add('button', undefined, 'ОК');
    w.grBtns.btnOK.onClick = function () {
        for (var v = 0; v < arrVarsNames.length; ++v)
            varsValues.push(w.pnlVars['grp' + v].edt.text);
        w.close();
    }
    w.grBtns.btnOK.active = true;
    w.show();

    return varsValues;
}

function resetValues(selection, varsValues) {
    // пока храним переменные в marker.label для совместимости со всем остальным
    // если в выделении несколько объектов задавать значение переменной сразу для всех
    for (var s = 0; s < selection.length; ++s) {
        var label = checkMess(selection[s].label).text;
        var oldVars = label.split(';\r');
        var vars = [];
        for (var v = 0; v < varsValues.length; ++v) {
            if (varsValues[v] === '[несколько значений]')
                vars.push(oldVars[v]);
            else
                vars.push(varsValues[v]);
        }
        var newLabel = vars.join(';\r');
        selection[s].label = newLabel;
    }
}
