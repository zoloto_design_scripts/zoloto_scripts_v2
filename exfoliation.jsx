﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');

var COMMON_LAYERS = {
    'подвесные план': true,
    'градиент для визуализаций': true,
    'осноной слой': true,
    'колонтитул': true,
    'help_moves': true,
    'help': true,
    'legend': true,
    'floor': true,
    'table': true,
    'map': true
};

app.doScript(main, undefined, undefined,
    UndoModes.entireScript, "Exfoliation");

function main() {
    var doc = app.activeDocument;
    var layers = doc.layers;
    var pages = doc.pages;
    var layersPages = checkForLayersObjectsOnPages(doc);
    // проходим по слоям и если слой не общий и виден и есть в индексе, 
    //  то копируем в конец номера страниц из индекса

    // как только страница скопирована запускаем для последней на текущей момент страницы
    //  удаление других слоёв
    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, layers.length);
    for (var l = 0; l < layers.length; ++l) {
        var L = layers[l];
        if (!L.visible || isCommonLayer(L.name) || layersPages[L.name] === undefined)
            continue;
        var pNums = layersPages[L.name];
        for (var n = 0; n < pNums.length; ++n) {
            var lastSpread = pages[pNums[n]].parent.duplicate(LocationOptions.AT_END);
            var lastPage = lastSpread.pages[0];
            dominatelayer(doc, L.name, lastPage.documentOffset);
        }
        progress.value++;
    }
    w.close();
    alert('Успешно');
}

function checkForLayersObjectsOnPages(doc) {
    // строим словарь вида {имя слоя -> 0-based индексы страниц где есть его объекты}


    function checkOneLayer(layer, pageCount) {
        var buf = Array(pageCount);
        for (var l = 0; l < buf.length; ++l)
            buf[l] = false;

        var spl = layer.splineItems;
        for (var s = 0; s < spl.length; ++s) {
            var p = spl[s].parentPage.documentOffset;
            buf[p] = true;
        }

        var poss = [];
        for (var o = 0; o < buf.length; ++o)
            if (buf[o])
                poss.push(o);
        return poss;
    }

    var index = {};
    var lay = doc.layers;
    for (var l = 0; l < lay.length; ++l) {
        var L = lay[l];
        if (L.visible)
            index[L.name] = checkOneLayer(L, doc.pages.length);
    }

    return index;
}

function dominatelayer(doc, layerName, pageNum) {
    function savedLayers(originLayerName, ckeckLayerName) {
        if (ckeckLayerName.length < originLayerName.length)
            return false;
        else {
            var startsWith = ckeckLayerName.substr(0, originLayerName.length);
            return (originLayerName === startsWith);
        }
    }

    // удаляем все сплайны на данной странице и не этом (или связанных) слоях
    var spl = doc.pages[pageNum].splineItems;
    var splToDelete = [];
    for (var s = 0; s < spl.length; ++s) {
        var S = spl[s];
        if (!isCommonLayer(S.itemLayer.name) && !savedLayers(layerName, S.itemLayer.name)) {
            splToDelete.push(S.id);
        }
    }
    for (var st = 0; st < splToDelete.length; ++st) {
        var S = doc.splineItems.itemByID(splToDelete[st]);
        S.locked = false;
        S.remove();
    }

    // аналогично для textframes
    var txf = doc.pages[pageNum].textFrames;
    var txfToDelete = [];
    for (var t = 0; t < txf.length; ++t) {
        var T = txf[t];
        if (!isCommonLayer(T.itemLayer.name) && !savedLayers(layerName, T.itemLayer.name)) {
            txfToDelete.push(T.id);
        }
    }
    for (var tt = 0; tt < txfToDelete.length; ++tt) {
        var T = doc.textFrames.itemByID(txfToDelete[tt]);
        T.locked = false;
        T.remove();
    }
}

function isCommonLayer(layerName) {
    var containUpper = (layerName.toLocaleLowerCase() !== layerName);

    var hasNumber = false;
    for (var i = 0; i <= 9; ++i) {
        if (layerName.indexOf(String(i)) !== -1)
            hasNumber = true;
    }

    // return COMMON_LAYERS[layerName];
    return !(containUpper && hasNumber);
}
