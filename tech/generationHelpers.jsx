﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/templates.jsx');

function replacePictsNames() {
    var docDirPath = docDir + placement;

    var pictPath = docDirPath + '/' + namePrefix + 'pict_codes.csv';
    // alert('Словарь пиктограмм ожидается по адресу:\n' + String(pictPath));
    var pictFile = getCSV(pictPath, 'picts');
    if (pictFile) {
        var pictDict = parseCSV(pictFile);
        replaceByMap(data, pictDict);
    }

    var namesPath = docDirPath + '/' + namePrefix + 'poi_names.csv';
    // alert('Словарь имён ожидается по адресу:\n' + String(namesPath));
    var namesFile = getCSV(namesPath);
    if (namesFile) {
        var namesDict = parseCSV(namesFile, 'names');
        replaceByMultimap(data, namesDict);
    }
}

function generatePreviews(previewDoc, templatesCasesDocs) {
    // импорт необходимых стилей шаблонов сразу из всех файлов
    for (var templateName in templatesCasesDocs) {
        var templateCasesDoc = templatesCasesDocs[templateName];
        var tFile = templateCasesDoc.fullName;
        var doNotLoad = GlobalClashResolutionStrategy.DO_NOT_LOAD_THE_STYLE; // метод разрешения конфликтов стилей
        previewDoc.importStyles(ImportFormat.CHARACTER_STYLES_FORMAT, tFile, doNotLoad);
        previewDoc.importStyles(ImportFormat.PARAGRAPH_STYLES_FORMAT, tFile, doNotLoad);
        previewDoc.importStyles(ImportFormat.OBJECT_STYLES_FORMAT, tFile, doNotLoad);
    }

    var pages = previewDoc.pages;
    var masterPages = [];
    for (var mp = 0; mp < pages.length; ++mp)
        masterPages.push(pages[mp]);

    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, data.length);

    for (var p = 0; p < data.length; ++p) {
        var vars = data[p];
        for (var m = 0; m < masterPages.length; ++m) {
            var page = masterPages[m].duplicate(LocationOptions.AT_END);

            if (useMasters(vars)) {
                var masterName = extractMasterFromVars(vars);
                try {
                    page.appliedMaster = previewDoc.masterSpreads.itemByName(masterName);
                } catch (e) {
                    alert('Мастер-страница для носителя ' + app.activeDocument.activeLayer.name +
                        ' с названием ' + masterName + ' отсутствует.\n' +
                        '\nСписок мастер-страниц в шаблоне:\n' +
                        allMasterSpreadsNames(previewDoc));
                }
            }

            if (!page.appliedMaster) continue;

            var fieldsFromMaster = getTextFrames(page.appliedMaster);
            for (var j = 0; j < fieldsFromMaster.length; j++) {
                try { fieldsFromMaster[j].override(page); }
                catch (e) { }
            }
            page.transform(CoordinateSpaces.INNER_COORDINATES, AnchorPoint.BOTTOM_CENTER_ANCHOR,
                page.masterPageTransform.invertMatrix()); // исправляем баг индизайна

            var fields = getTextFrames(page);
            for (var f = 0; f < fields.length; ++f) {
                var field = fields[f];
                varsSubstitutionNew(field, vars);
                applyParagraphStyles(field);
            }
        }
        progress.value++;
    }

    for (var m = masterPages.length - 1; m >= 0; --m)
        masterPages[m].remove();
    previewDoc.save();
    return previewDoc.pages.length;
}

function applyParagraphStyles(field) {
    var paragraphs = field.paragraphs;
    if (paragraphs.length > 0) {
        var firstParagraph = paragraphs[0];
        var currentParagraphStyle = firstParagraph.appliedParagraphStyle;

        for (var i = 0; i < paragraphs.length; i++) {
            if (!currentParagraphStyle) {
                alert('Не указан или не выбран стиль параграфа');
                break;
            }
            paragraphs[i].applyParagraphStyle(currentParagraphStyle);
            currentParagraphStyle = currentParagraphStyle.nextStyle;
        }
    }
}

function useMasters(vars) {
    firstVar = vars[0];
    if (firstVar.indexOf('mp:') !== -1)
        return true;
    else
        return false;
}

function extractMasterFromVars(vars) {
    var firstVar = vars.shift();
    var position = firstVar.indexOf('mp:');
    firstVar = firstVar.slice(position + 3);
    firstVar = firstVar.replace(/(^\s+|\s+$)/g, ''); // обрезаем пробелы с двух сторон
    if (firstVar.split(' ').length == 2) {
        firstVar = firstVar.split(' ');
        firstVar = firstVar.join('-');
    }
    return firstVar;
}

function varsSubstitutionNew(field, vars) {
    app.findTextPreferences = NothingEnum.nothing;
    app.changeTextPreferences = NothingEnum.nothing;
    app.findChangeTextOptions.caseSensitive = true;
    app.findChangeTextOptions.wholeWord = true;
    if (vars.number && field.contents.indexOf('%%num') !== -1) {
        app.findTextPreferences.findWhat = '%%num';
        app.changeTextPreferences.changeTo = vars.number;
        field.changeText();
    }

    while (field.contents.indexOf('%%') !== -1) {
        app.findTextPreferences = NothingEnum.nothing;
        app.changeTextPreferences = NothingEnum.nothing;
        var N = 0;
        var i = field.contents.indexOf('%%') + 2;
        while (i < field.contents.length && '0123456789'.indexOf(field.contents[i]) !== -1) {
            N *= 10;
            N += Number(field.contents[i]);
            ++i;
        }
        if (N > 0 && N <= vars.length) {
            app.findTextPreferences.findWhat = '%%' + N;
            app.changeTextPreferences.changeTo = vars[N - 1];
            field.changeText();
        } else {
            app.findTextPreferences.findWhat = '%%' + N;
            app.changeTextPreferences.changeTo = '';
            field.changeText();
        }
    }
}

function getTextFrames(page) {
    var buf = [];
    var items = page.allPageItems;
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        if (item.reflect.name === "TextFrame")
            buf.push(item);
    }
    return buf;
}

function allMasterSpreadsNames(document) {
    var result = [];
    var name;
    for (var i = 0; i < document.masterSpreads.length; i++) {
        name = document.masterSpreads[i].name;
        result.push(name + '\n');
    }
    return result.join('');
}