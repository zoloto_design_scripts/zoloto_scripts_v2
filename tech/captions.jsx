﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');

function createCaptionLayer(layers, origName, origColor, suffix) {
    var captionsLayerName = [origName, suffix].join('_');
    var captionsLayer = layers.itemByName(captionsLayerName);
    if (!captionsLayer.isValid) {
        captionsLayer = layers.add({ name: captionsLayerName, layerColor: origColor });
        alert('Создан слой \'' + captionsLayerName + '\'.');
    }
    return captionsLayer;
}
function createCaptionLayerCnd(layers, origName, origColor, suffix, silently) {
    var captionsLayerName = [origName, suffix].join('_');
    var captionsLayer = layers.itemByName(captionsLayerName);
    if (!captionsLayer.isValid) {
        captionsLayer = layers.add({ name: captionsLayerName, layerColor: origColor });
        if (!silently)
            alert('Создан слой \'' + captionsLayerName + '\'.');
    }
    return captionsLayer;
}

function linkBroken(captionsContainer, captionId, markerId) {
    var caption = captionsContainer.itemByID(captionId);
    return (!caption.isValid || caption.extractLabel('MarkerId') !== String(markerId));
}

function paintOnColorError(caption, marker) {
    var lblExpColor = marker.extractLabel('ExpectingColorName');
    if (lblExpColor !== undefined && lblExpColor !== '') {
        if (marker.fillColor.name !== lblExpColor) {
            // var doc = caption.parentPage.parent.parent;
            /*
            var C = app.colors;
            var buf = [];
            for (var i = 0; i < C.length; ++i) {
                buf.push(C[i].name);
            }
            $.writeln(buf);
            */
            caption.fillColor = app.colors.itemByName('Magenta').name;
            return true;

        }
    }
    return false;
}

function appendError(index, marker) {
    var offset = marker.parentPage.documentOffset + 1;
    if (index[offset] === undefined)
        index[offset] = 1;
    else
        index[offset] += 1;

    if (index.total === undefined)
        index.total = 1;
    else
        index.total += 1;
}
function generateErrReport(index) {
    var msg = '';
    for (var key in index) {
        if (index.hasOwnProperty(key) && key != 'total') {
            msg += 'страница ' + key + ' - ' + 'ошибки ' + index[key] + '\n';
        }
    }
    return msg;
}


function generateCaptions(markers, captionsLayer, styles, captionType, checkColorRaw) {
    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, markers.length);

    var checkColor = (checkColorRaw !== undefined ? true : false);
    var colorErrors = {};

    var captions = captionsLayer.textFrames;
    for (var i = 0; i < markers.length; ++i) {
        var marker = markers[i];
        var lblCaptionBoxId = marker.extractLabel(captionType + 'BoxId');
        var captionBoxId = Number(lblCaptionBoxId);
        if (!lblCaptionBoxId || lblCaptionBoxId === '' || linkBroken(captions, captionBoxId, marker.id)) {
            var caption = createCaption(marker, styles, captionsLayer);
            marker.insertLabel(captionType + 'BoxId', caption.id.toString());
        }
        else
            var caption = captions.itemByID(captionBoxId);
        modifyCaption(caption, marker);

        var isError = (checkColor ? paintOnColorError(caption, marker) : false);
        if (isError)
            appendError(colorErrors, marker);

        progress.value++;
    }

    progress.maxvalue = captions.length;
    progress.value = captions.length;
    for (var l = 0; l < captions.length; ++l) {
        var caption = captions[l];
        var lblMarkerId = caption.extractLabel('MarkerId');
        if (!lblMarkerId || lblMarkerId === '')
            caption.remove();
        else {
            var itsMarker = markers.itemByID(Number(lblMarkerId));
            if (!itsMarker.isValid || itsMarker.extractLabel(captionType + 'BoxId') !== String(caption.id))
                caption.remove();
        }
        progress.value--;
    }

    w.close();

    if (captionType === 'Number') {
        if (colorErrors.total !== undefined && colorErrors.total > 0) {
            alertMessage += 'Пронумеровано ' + markers.length + ' объектов.\n'
                + 'Ошибочный цвет на страницах\n'
                + generateErrReport(colorErrors);
        } else {
            alertMessage += 'Пронумеровано '
                + markers.length + ' объектов.\n';
        }

    }
    else {
        if (colorErrors.total !== undefined && colorErrors.total > 0)
            alertMessage += 'Ошибочный цвет на страницах\n' + generateErrReport(colorErrors);
    }
}

// function createCaption(  marker: SplineItem, 
//                          styles: Object, 
//                          captionsLayer: Layer) -> tasksCaption: TextFrame
// function modifyCaption(caption: TextFrame, marker: SplineItem) -> void
function generateCaptionsExplicit(markers, captionsLayer, styles,
    captionType, createCaption, modifyCaption) {
    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, markers.length);

    var captions = captionsLayer.textFrames;
    for (var i = 0; i < markers.length; ++i) {
        var marker = markers[i];
        var lblCaptionBoxId = marker.extractLabel(captionType + 'BoxId');
        var captionBoxId = Number(lblCaptionBoxId);
        if (!lblCaptionBoxId || lblCaptionBoxId === '' || linkBroken(captions, captionBoxId, marker.id)) {
            var caption = createCaption(marker, styles, captionsLayer);
            marker.insertLabel(captionType + 'BoxId', caption.id.toString());
        }
        else
            var caption = captions.itemByID(captionBoxId);
        modifyCaption(caption, marker);
        progress.value++;
    }

    progress.maxvalue = captions.length;
    progress.value = captions.length;
    for (var l = 0; l < captions.length; ++l) {
        var caption = captions[l];
        var lblMarkerId = caption.extractLabel('MarkerId');
        if (!lblMarkerId || lblMarkerId === '')
            caption.remove();
        else {
            var itsMarker = markers.itemByID(Number(lblMarkerId));
            if (!itsMarker.isValid || itsMarker.extractLabel(captionType + 'BoxId') !== String(caption.id))
                caption.remove();
        }
        progress.value--;
    }

    w.close();
}
