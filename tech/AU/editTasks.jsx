﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech//tech/

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/files.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/JSON-js-master/json2.js');

var NUMBER = 'Number';
var TASKS = 'Tasks';

var taskFileName = '/user_tasks.json';

function main() {
    var doc = app.activeDocument;
    var dir = doc.filePath;
    if (doc.selection.length !== 1) {
        alert('editTasks\nдолжен быть выбран один объект');
        return;
    }
    var au = doc.selection[0];

    var lblAUTasks = au.extractLabel(TASKS);
    var taskArr = (lblAUTasks && lblAUTasks !== '' ? eval(lblAUTasks) : []);

    var tasksFile = new File(dir + taskFileName);
    var tasksJson = parseTasks(tasksFile);
    var tasks = collectTasks(tasksJson);

    var checkboxes = [];
    var newTaskArr = [];
    var newTaskItems = [];
    //#targetengine "session"; 
    var w = new Window('dialog {orientation: "column", alignChildren: "left"}', 'Add task');
    pnl = w.add('panel', undefined, undefined, 'Tasks', {});
    pnl.maximumSize.height = 500;
    var scrollItems = pnl.add('group {orientation: "column", alignChildren: "left"}');
    for (var t = 0; t < tasks.length; ++t) {
        var ch = scrollItems.add('checkbox', undefined, tasks[t]);
        ch.value = (indexOf(taskArr, tasks[t]) !== -1);
        checkboxes.push(ch);
    }

    var scroll = pnl.add('scrollbar {stepdelta: 20}');
    scroll.onChanging = function () {
        scrollItems.location.y = -1 * this.value;
    }

    function showHandler() {
        scroll.size.height = pnl.size.height - 20;
        scroll.size.width = 20;
        scroll.location = [pnl.size.width - 30, 8];
        scroll.maxvalue = scrollItems.windowBounds[3] - pnl.size.height;
    }
    w.onShow = showHandler;

    var grAdd = w.add('group {orientation: "row", alignChildren: "left"}');
    grAdd.add('statictext', undefined, 'Task: ');
    var addTask = grAdd.add('edittext', [0, 0, 150, 20]);
    var btnAdd = grAdd.add('button', undefined, 'add');
    btnAdd.onClick = function () {
        var newTask = addTask.text;
        tasks.push(newTask);
        newTaskItems.push(newTask);
        var ch = scrollItems.add('checkbox', undefined, newTask);
        ch.value = true;
        checkboxes.push(ch);
        w.layout.layout(true);
        showHandler();
    }

    w.grBtns = w.add('group');
    w.grBtns.alignment = 'right';
    w.grBtns.btnOK = w.grBtns.add('button', undefined, 'ОК');
    w.grBtns.btnOK.onClick = function () {
        for (var c = 0; c < tasks.length; ++c)
            if (checkboxes[c].value)
                newTaskArr.push(tasks[c]);
        au.insertLabel(TASKS, newTaskArr.toSource());
        w.close();
    }
    w.grBtns.btnOK.active = true;
    w.show();

    appendTaskItems(tasksJson, newTaskItems, tasksFile);
    tasksFile.close();
}


function parseTasks(tasksFile) {
    if (!tasksFile.exists)
        return {};
    tasksFile.open('e');
    tasksFile.encoding = 'UTF-8';
    var content = tasksFile.read();
    return JSON.parse(content);
}

function collectTasks(tasksJson) {
    var taskArr = [];
    // tasksJson: [{UserGoals: [{GoalTasks: [TaskName]}]  }]
    for (var u = 0; u < tasksJson.length; ++u) {
        var userObj = tasksJson[u];
        var userGoals = userObj.userGoals;
        for (var g = 0; g < userGoals.length; ++g) {
            var goalObj = userGoals[g];
            var goalTasks = goalObj.goalTasks;
            for (var t = 0; t < goalTasks.length; ++t)
                taskArr.push(goalTasks[t]);
        }
    }
    return taskArr;
}

//todo
function appendTaskItems(tasksJson, newTaskItems, tasksFile) {
    var noGoal = { goalName: 'unknown', goalTasks: newTaskItems };
    var newUser = { userName: 'unknown', userCriteria: '', userGoals: [noGoal] }
    tasksJson.push(newUser);

    tasksFile.seek(0);
    tasksFile.encoding = 'UTF-8';
    tasksFile.write(JSON.stringify(tasksJson));
}

main();
