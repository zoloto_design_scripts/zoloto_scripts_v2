﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech//tech/

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/files.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/JSON-js-master/json2.js');

var NUMBER = 'Number';
var TASKS = 'Tasks';
var FUNCS = 'Functions';

var rawCSV = '/au_data.csv';
var typeJSON = '/au_data_by_type.json';
var typesCSV = '/types.csv';

function Func(funcArray) {
    this.data = [];
    function stripSpaces(line) {
        var i = 0;
        while (i < line.length && (line[i] === ' ' || line[i] === '\t'))
            ++i; //i индекс первого не пробела
        var j = line.length - 1;
        while (j >= 0 && (line[j] === ' ' || line[j] === '\t'))
            --j; // j индекс последнего не пробела
        return line.slice(i, j + 1);
    }
    for (var i = 0; i < funcArray.length; ++i)
        this.data.push(stripSpaces(funcArray[i]));
}
Func.prototype.toJSON = function (key) {
    return this.data.join(', ');
}

function main() {
    var doc = app.activeDocument;
    var dir = doc.filePath;
    var layer = doc.activeLayer;

    var taskDict = {};  // task -> num, auType, action, args
    var typeDict = {};  // auType -> {carrType -> [func]}
    var auArr = layer.splineItems;
    for (var i = 0; i < auArr.length; ++i) {
        var au = auArr[i];
        var id = au.id;
        var num = au.extractLabel(NUMBER);
        if (!num) {
            alert('на одном из AU не найдена нумерация');
            return;
        }
        num = num.split('/')[0];
        var auType = au.fillColor.name;

        var lblAUTasks = au.extractLabel(TASKS);
        var taskArr = (lblAUTasks ? eval(lblAUTasks) : []);

        var lblAUFunks = au.extractLabel(FUNCS);
        var funcArr = (lblAUFunks ? eval(lblAUFunks) : []);
        var taskFunctions = {};
        for (var f = 0; f < funcArr.length; ++f) {
            var funcObj = funcArr[f];
            var task = funcObj.task;
            var func = funcObj.func;
            var fID = String(id) + ':' + String(f);
            var head = [fID, String(funcObj.carrType)];
            func = head.concat(func);
            var carrType = funcObj.carrType;
            if (taskFunctions[task] === undefined)
                taskFunctions[task] = [];
            taskFunctions[task].push(func);

            if (typeDict[auType] === undefined)
                typeDict[auType] = {};
            if (typeDict[auType][carrType] === undefined)
                typeDict[auType][carrType] = [];
            typeDict[auType][carrType].push(new Func(func));
        }

        // обходим массив тасков и ...
        for (var t = 0; t < taskArr.length; ++t) {
            var task = taskArr[t];
            if (taskDict[task] === undefined)
                taskDict[task] = [];
            // если нет маппинга в функции кладем прототип -- пустую строку
            var functions = taskFunctions[task];
            if (functions !== undefined) {
                for (var u = 0; u < functions.length; ++u) {
                    var ent = [id, num, auType].concat(functions[u]);
                    taskDict[task].push(ent);
                }
            }
            else {
                var ent = [id, num, auType].concat(['', '', '', '', '', '', '', '']);
                taskDict[task].push(ent);
            }
        }
    }
    typeDict = filterUnique(typeDict);
    var swatchInfo = collectSwatchInfo(doc);

    var csvFile = dir + rawCSV;
    var header = ['id', 'Task', 'AU', 'AUType', 'idF', 'CarrType', 'Action', 'Data'];
    writeFile(csvFile, toCSV(header, taskDict), 'UTF-8');

    var jsonFile = dir + typeJSON;
    writeFile(jsonFile, JSON.stringify(typeDict, undefined, 2), 'UTF-8');

    var auTypeInfoFile = dir + typesCSV;
    var swHeader = ['id', 'Type'];
    writeFile(auTypeInfoFile, matrToCSV(swHeader, swatchInfo), 'UTF-8');

    alert(['Exported', File(csvFile).fullName, File(jsonFile).fullName].join('\n'));
}

function matrToCSV(header, body) {
    var lines = [header];
    for (var i = 0; i < body.length; ++i)
        lines.push(body[i].join(','));
    return lines.join('\n');
}

function toCSV(header, taskDict) {
    var matrix = [];
    for (var task in taskDict) {
        var entArr = taskDict[task];
        for (var e = 0; e < entArr.length; ++e) {
            var ent = entArr[e];
            var head = [ent.splice(0, 1), (e === 0 ? task : '')];
            matrix.push(head.concat(ent));
        }

    }

    function alignColumn(matr, j) {
        // найти длины слов, max(round up 4), min
        // add n\t, n = max - (длина - min)
        var lArr = [];
        var max = 0;
        var min = 1000;
        for (var i = 0; i < matr.length; ++i) {
            var len = matr[i][j].length;
            lArr.push(len);
            if (len > max)
                max = len;
            if (len < min)
                min = len;
        }
        max += 2;
        min += 2; // запас для кавычек
        max = Math.ceil(max / 4) * 4;

        function genNtab(n) {
            var buf = new Array(n + 1);
            return buf.join('\t');
        }
        for (var i = 0; i < matr.length; ++i) {
            var n = max - (lArr[i] - min);
            n = Math.ceil(n / 8);
            matr[i][j] = matr[i][j] + genNtab(n);
        }
    }
    if (matrix.length > 0) {
        var nCol = matrix[0].length;
        for (var j = 0; j < nCol; ++j)
            alignColumn(matrix, j);
    }

    var content = matrToCSV(header.join(','), matrix);
    return content;
}

function collectSwatchInfo(doc) {
    var matrix = [];
    var swatches = doc.swatches;
    for (var s = 0; s < swatches.length; ++s) {
        matrix[s] = [swatches[s].id, swatches[s].name];
    }
    return matrix;
}

function filterUnique(typeDict) {
    // typeDict: auType -> {carrType -> [func]}
    function filter(funcArr) {
        var dataHash = {};
        var count = 0
        function getHash(data) {
            if (dataHash[data] === undefined) {
                var hash = count;
                count += 1;
                dataHash[data] = hash;
                return hash;
            }
            else
                return dataHash[data];
        }
        function funcHash(func) {
            var data = func.data;
            return String(getHash(data[0]) + 1000 * getHash(data[1])) +
                String(getHash(data[2]) + 1000 * getHash(data[3])) +
                String(getHash(data[4]) + 1000 * getHash(data[5]));
        }
        var uniq = {};
        for (var f = 0; f < funcArr.length; ++f)
            uniq[funcHash(funcArr[f])] = funcArr[f];
        var res = [];
        for (var hash in uniq)
            res.push(uniq[hash]);
        return res;
    }
    var uniq = {};
    for (var type in typeDict) {
        if (uniq[type] == undefined)
            uniq[type] = {}
        for (var carrType in typeDict[type])
            uniq[type][carrType] = filter(typeDict[type][carrType]);
    }
    return uniq;
}

main();
