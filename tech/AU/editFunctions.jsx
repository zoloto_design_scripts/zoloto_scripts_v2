﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech /

    $.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/files.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');

var NUMBER = 'Number';
var TASKS = 'Tasks';
var FUNCS = 'Functions';

var actionFileName = '/action.list.txt';
var dataFileName = '/data.list.txt';

function main() {
    var doc = app.activeDocument;
    var dir = doc.filePath;

    if (doc.selection.length !== 1) {
        alert('editFunction\nдолжен быть выбран один объект');
        return;
    }
    var au = doc.selection[0];

    var num = au.extractLabel(NUMBER);
    if (!num || num === '') {
        alert('не найдена нумерация');
        return;
    }
    var lblAUTasks = au.extractLabel(TASKS);
    if (!lblAUTasks || lblAUTasks === '') {
        alert(num + 'не настроены task');
        return;
    }
    var tasks = eval(lblAUTasks);
    var actionsFile = new File(dir + actionFileName);
    actionsFile.open('r');
    var actions = parseList(actionsFile);
    actionsFile.close();
    if (!actions)
        return;
    var dataFile = new File(dir + dataFileName);
    dataFile.open('e');
    var data = parseList(dataFile);
    if (!data)
        return;
    var lblAUFunks = au.extractLabel(FUNCS);
    var funcArr = (lblAUFunks && lblAUFunks !== '' ? eval(lblAUFunks) : []);

    var func = undefined;
    var newDataItems = [];
    var w = new Window('dialog', num + ' functions');
    w.pnlFunc = w.add('panel', undefined, undefined, 'Functions');
    if (funcArr.length === 0)
        w.pnlFunc.add('statictext', undefined, 'пока не заданы');
    else {
        function callbackFabric(i) {
            var callback = function () {
                funcArr.splice(i, 1);
                w.close();
            }
            return callback;
        }

        for (var i = 0; i < funcArr.length; ++i) {
            gr = w.pnlFunc.add('group');
            gr.add('statictext', undefined, funcToStr(funcArr[i]));
            rmvBtn = gr.add('button', undefined, 'Убрать');
            rmvBtn.onClick = callbackFabric(i);
        }
    }

    w.grWork = w.add('group');
    w.grWork.tsk = w.grWork.add('listbox', undefined, undefined, { items: tasks });
    w.grWork.lbl = w.grWork.add('statictext', undefined, ' | ');
    w.grWork.act = w.grWork.add('listbox', undefined, undefined, { items: actions });
    w.grWork.pn1 = w.grWork.add('panel', undefined, 'arg1');
    w.grWork.pn1.lst = w.grWork.pn1.add('dropdownlist', undefined, undefined, { items: data });
    w.grWork.pn1.edt = w.grWork.pn1.add('edittext', { x: 0, y: 0, width: 156, height: 23 });
    w.grWork.pn2 = w.grWork.add('panel', undefined, 'arg2');
    w.grWork.pn2.lst = w.grWork.pn2.add('dropdownlist', undefined, undefined, { items: data });
    w.grWork.pn2.edt = w.grWork.pn2.add('edittext', { x: 0, y: 0, width: 156, height: 23 });
    w.grWork.pn3 = w.grWork.add('panel', undefined, 'arg3');
    w.grWork.pn3.lst = w.grWork.pn3.add('dropdownlist', undefined, undefined, { items: data });
    w.grWork.pn3.edt = w.grWork.pn3.add('edittext', { x: 0, y: 0, width: 156, height: 23 });
    w.grWork.pn4 = w.grWork.add('panel', undefined, 'arg4');
    w.grWork.pn4.lst = w.grWork.pn4.add('dropdownlist', undefined, undefined, { items: data });
    w.grWork.pn4.edt = w.grWork.pn4.add('edittext', { x: 0, y: 0, width: 156, height: 23 });
    w.grWork.pn5 = w.grWork.add('panel', undefined, 'arg5');
    w.grWork.pn5.lst = w.grWork.pn5.add('dropdownlist', undefined, undefined, { items: data });
    w.grWork.pn5.edt = w.grWork.pn5.add('edittext', { x: 0, y: 0, width: 156, height: 23 });
    w.grBtns = w.add('group');
    w.grBtns.alignment = 'right';
    w.grBtns.btnCancel = w.grBtns.add('button', undefined, 'Отмена');
    w.grBtns.btnCancel.onClick = function () {
        w.close();
    }
    w.grBtns.btnOK = w.grBtns.add('button', undefined, 'ОК');
    w.grBtns.btnOK.onClick = function () {
        func = collectFunc(w.grWork, newDataItems);
        if (!func)
            alert('нужно задать task и action');
        else
            w.close();
    }
    w.grBtns.btnOK.active = true;
    w.show();

    if (func) {
        funcArr.push(func);
        appendDataItems(newDataItems, dataFile);
        dataFile.close();
    }

    au.insertLabel(FUNCS, funcArr.toSource());
    var buf = [];
    for (var i = 0; i < funcArr.length; ++i)
        buf.push(funcToMsg(funcArr[i]));
    au.label = buf.join('\n');
    if (func)
        alert('Added\n' + funcToStr(func));
}

function collectFunc(grFunc, newDataItems) {
    // grFunc.tsk
    // grFunc.act
    // grFunc.pn[12345]
    // grFunc.pn[12345].lst
    // grFunc.pn[12345].edt
    function getArg(pnl) {
        var edtVal = pnl.edt.text;
        if (edtVal !== '') {
            newDataItems.push(edtVal);
            return edtVal;
        }
        var lstSel = pnl.lst.selection;
        if (lstSel !== null)
            return lstSel.text;
        return '';
    }

    var tsk = grFunc.tsk.selection;
    var act = grFunc.act.selection;
    if (tsk === null || act === null)
        return undefined;
    return {
        task: tsk.text, func: [act.text, getArg(grFunc.pn1), getArg(grFunc.pn2),
        getArg(grFunc.pn3), getArg(grFunc.pn4), getArg(grFunc.pn5)]
    };
}

function funcToStr(func) {
    return func.task + '|' + func.func[0] + '(' + func.func[1] + ',' +
        func.func[2] + ',' + func.func[3] + ',' + func.func[4] + ',' + func.func[5] + ')'
}

function funcToMsg(func) {
    return func.func[0] + '\t' + func.func[1] + '\t' + func.func[2] + '\t'
        + func.func[3] + '\t' + func.func[4] + '\t' + func.func[5];
}

function appendDataItems(newDataItems, dataFile) {
    newDataItems.push('');
    dataFile.encoding = 'UTF-8';
    dataFile.write(newDataItems.join('\n'));
}

main();
