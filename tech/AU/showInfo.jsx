﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/enumerate.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/geometry.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/captions.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/styles.jsx');

var TASKS = 'Tasks';
var FUNCS = 'Functions';
var CRTPS = 'CarrierTypes';

function main() {
    var doc = app.activeDocument;
    var layer = doc.activeLayer;
    var origName = layer.name;
    var markers = layer.splineItems;

    var styles = getMessStyles(doc.paragraphStyles, doc.objectStyles, origName);
    if (!styles)
        return;

    var tasksLayer = createCaptionLayerCnd(doc.layers, origName, layer.layerColor, 'tasks', true);
    generateCaptionsExplicit(markers, tasksLayer, styles,
        'Tasks', createTasksCaption, modifyTasksCaption);

    var funcsLayer = createCaptionLayerCnd(doc.layers, origName, layer.layerColor, 'funcs', true);
    generateCaptionsExplicit(markers, funcsLayer, styles,
        'Funcs', createFuncsCaption, modifyFuncsCaption);

    var crTpsLayer = createCaptionLayerCnd(doc.layers, origName, layer.layerColor, 'carr_types', true);
    generateCaptionsExplicit(markers, crTpsLayer, styles,
        'CarrierTypes', createCrTypesCaption, modifyCrTypesCaption);

    doc.activeLayer = layer;
    doc.save();
}

function pointTop(oval) {
    var bounds = oval.geometricBounds;
    var top = bounds[0];
    var left = bounds[1];
    var right = bounds[3];
    return [(left + right) / 2., top];
}

function pointBottom(oval) {
    var bounds = oval.geometricBounds;
    var left = bounds[1];
    var bottom = bounds[2];
    var right = bounds[3];
    return [(left + right) / 2., bottom];
}

function pointRight(oval) {
    var bounds = oval.geometricBounds;
    var top = bounds[0];
    var bottom = bounds[2];
    var right = bounds[3];
    return [right, (top + bottom) / 2.];
}


function createTasksCaption(marker, styles, captionsLayer) {
    var tasksCaption = marker.parentPage.textFrames.add();
    tasksCaption.move(captionsLayer);
    tasksCaption.insertLabel('MarkerId', marker.id.toString());

    tasksCaption.applyObjectStyle(styles.object);
    tasksCaption.parentStory.texts[0].applyParagraphStyle(styles.paragraph);

    var point = pointTop(marker);
    tasksCaption.geometricBounds = [point[1], point[0] - 2,
    point[1] + 4, point[0] + 2];

    return tasksCaption;
}

function createFuncsCaption(marker, styles, captionsLayer) {
    var funcsCaption = marker.parentPage.textFrames.add();
    funcsCaption.move(captionsLayer);
    funcsCaption.insertLabel('MarkerId', marker.id.toString());

    funcsCaption.applyObjectStyle(styles.object);
    funcsCaption.parentStory.texts[0].applyParagraphStyle(styles.paragraph);

    var point = pointBottom(marker);
    funcsCaption.geometricBounds = [point[1], point[0] - 2,
    point[1] + 4, point[0] + 2];

    return funcsCaption;
}

function createCrTypesCaption(marker, styles, captionsLayer) {
    var crTypesCaption = marker.parentPage.textFrames.add();
    crTypesCaption.move(captionsLayer);
    crTypesCaption.insertLabel('MarkerId', marker.id.toString());

    crTypesCaption.applyObjectStyle(styles.object);
    crTypesCaption.parentStory.texts[0].applyParagraphStyle(styles.paragraph);

    var point = pointRight(marker);
    crTypesCaption.geometricBounds = [point[1], point[0] - 2,
    point[1] + 4, point[0] + 2];

    return crTypesCaption;
}

function modifyTasksCaption(caption, marker) {
    var mess = '';
    var lblAUTasks = marker.extractLabel(TASKS);
    if (!lblAUTasks || lblAUTasks === '')
        mess = 'не настроены task';
    else {
        var taskArr = eval(lblAUTasks);
        mess = taskArr.join('\n');
    }

    if (caption.contents !== mess) {
        caption.contents = mess;
        caption.fit(FitOptions.FRAME_TO_CONTENT);
    }
}

function funcToMsg(func) {
    var debug = [];
    return func.func[0] + '\t' + func.func[1] + '\t' + func.func[2] + '\t'
        + func.func[3] + '\t' + func.func[4] + '\t' + func.func[5];
}
function modifyFuncsCaption(caption, marker) {
    var mess = '';
    var lblAUFuncs = marker.extractLabel(FUNCS);
    if (!lblAUFuncs || lblAUFuncs === '')
        mess = 'не заданы function';
    else {
        var funcArr = eval(lblAUFuncs);
        var funcLines = [];
        for (var f = 0; f < funcArr.length; ++f)
            funcLines.push(funcToMsg(funcArr[f]));
        mess = funcLines.join('\n');
    }

    if (caption.contents !== mess) {
        caption.contents = mess;
        caption.fit(FitOptions.FRAME_TO_CONTENT);
    }
}

function modifyCrTypesCaption(caption, marker) {
    var mess = '';
    var lblCrTypes = marker.fillColor.extractLabel(CRTPS);
    if (!lblCrTypes || lblCrTypes === '')
        mess = 'не установлены типы носителей';
    else {
        var crTpsArr = eval(lblCrTypes);
        mess = crTpsArr.join('\n');
    }

    if (caption.contents !== mess) {
        caption.contents = mess;
        caption.fit(FitOptions.FRAME_TO_CONTENT);
    }
}

main();
