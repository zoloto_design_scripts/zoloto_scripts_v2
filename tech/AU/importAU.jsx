﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech /

    $.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/JSON-js-master/json2.js');

var TASKS = 'Tasks';
var FUNCS = 'Functions';
var CRTPS = 'CarrierTypes';

var taskFileName = '/task.list.txt';
var rawCSV = '/au_data.csv';
var typesCSV = '/types.csv';
var auTypeCarrType = '/au_data_by_type.json';

function main() {
    var doc = app.activeDocument;
    var dir = doc.filePath;

    var csvFile = new File(dir + rawCSV);
    if (!csvFile.exists) {
        alert('import\nне найден файл для импорта');
        return;
    }
    csvFile.open('e');
    csvFile.encoding = 'UTF-8';

    var auCarrFile = new File(dir + auTypeCarrType);
    var jsonData = {};
    if (auCarrFile.exists) {
        jsonData = parseJsonFile(auCarrFile);
        var func2CarrType = collectCarrTypes(jsonData);
        var matrix = parseCSV(csvFile);
        cutHeader(matrix)
        propagateCarrType(matrix, func2CarrType);
        csvFile.seek(0);
        csvFile.write(toCSV(matrix));
        csvFile.seek(0);
    }
    importCarrTypeHints(doc, jsonData);

    var matrixUpd = parseCSV(csvFile);
    csvFile.close();
    cutHeader(matrixUpd)

    var tasks = collectTasks(matrixUpd);
    var tasksFile = new File(dir + taskFileName);
    updateTasks(tasksFile, tasks);

    var auTypeInfoFile = new File(dir + typesCSV);
    if (!auTypeInfoFile.exists) {
        alert('import\nне найден файл types.csv\nswatch не обновлены');
    }
    else {
        auTypeInfoFile.open('r');
        auTypeInfoFile.encoding = 'UTF-8';
        var swatchInfo = parseCSV(auTypeInfoFile);
        var SC = updateSwatchNames(doc, swatchInfo);
        auTypeInfoFile.close();
    }

    var itemData = sortPerItem(matrixUpd);
    var C = importData(doc, itemData);

    alert('import\n'
        + C[0] + ' tasks\n'
        + C[1] + ' functions'
        + (SC ? '\n' + SC + ' swatch updated' : ''));
}

function parseCSV(csvFile) {
    var lines = csvFile.read().split('\n');
    while (lines[lines.length - 1] === '') {
        lines.pop;
        --lines.length;
    }
    var matrix = [];
    for (var l = 0; l < lines.length; ++l) {
        var parts = lines[l].split(',');
        for (var p = 0; p < parts.length; ++p)
            parts[p] = stripSpaces(parts[p]);
        matrix.push(parts);
    }
    return matrix;
}

function updateSwatchNames(doc, swatchInfo) {
    // swatchInfo = [ [id, name] ]
    var counter = 0;
    for (var i = 1; i < swatchInfo.length; ++i) {
        var swatchInput = swatchInfo[i];
        var swatch = doc.swatches.itemByID(Number(swatchInput[0]));
        if (!swatch.isValid)
            continue;
        if (swatch.name !== swatchInput[1]) {
            swatch.name = swatchInput[1];
            ++counter;
        }
    }
    return counter;
}

function propagateCarrType(matrix, func2CarrType) {
    // matrix[:][4]: fID
    // func2CarrType: fID -> carrType
    for (var i = 0; i < matrix.length; ++i) {
        var fId = matrix[i][4];
        var carrType = func2CarrType[fId];
        if (carrType)
            matrix[i][5] = carrType;
    }
}

function toCSV(matrix) {
    function alignColumn(matr, j) {
        // найти длины слов, max(round up 4), min
        // add n\t, n = max - (длина - min)
        var lArr = [];
        var max = 0;
        var min = 1000;
        for (var i = 0; i < matr.length; ++i) {
            var len = matr[i][j].length;
            lArr.push(len);
            if (len > max)
                max = len;
            if (len < min)
                min = len;
        }
        max += 2;
        min += 2; // запас для кавычек
        max = Math.ceil(max / 4) * 4;

        function genNtab(n) {
            var buf = new Array(n + 1);
            return buf.join('\t');
        }
        for (var i = 0; i < matr.length; ++i) {
            var n = max - (lArr[i] - min);
            n = Math.ceil(n / 8);
            matr[i][j] = matr[i][j] + genNtab(n);
        }
    }
    if (matrix.length > 0) {
        var nCol = matrix[0].length;
        for (var j = 0; j < nCol; ++j)
            alignColumn(matrix, j);
    }

    var header = ['id', 'Task', 'AU', 'AUType', 'idF', 'CarrType', 'Action', 'Data'].join(',');
    var lines = [header];
    for (var i = 0; i < matrix.length; ++i)
        lines.push(matrix[i].join(','));
    return lines.join('\n');
}

function cutHeader(matrix) {
    if (matrix[0][0] === 'id' || matrix[0][1] === 'Task'
        || matrix[0][2] === 'AU' || matrix[0][3] === 'AUType')
        matrix.splice(0, 1);
}

function collectTasks(matrix) {
    if (matrix.length < 1 || matrix[0].length < 2)
        return [];
    var taskExportedArr = [];
    for (var i = 0; i < matrix.length; ++i) {
        var task = matrix[i][1];
        if (task !== '') {
            if (indexOf(taskExportedArr, task) === -1)
                taskExportedArr.push(task);
        }
    }
    return taskExportedArr;
}

function updateTasks(tasksFile, exportedTasks) {
    var tasks = exportedTasks;
    if (tasksFile.exists) {
        tasksFile.open('e');
        var existingTasks = parseList(tasksFile);
        for (var t = 0; t < existingTasks.length; ++t)
            if (indexOf(tasks, existingTasks[t]) === -1)
                tasks.push(existingTasks[t]);
        tasksFile.seek(0);
    }
    else {
        tasksFile.open('w');
    }
    tasks.push('');
    tasksFile.write(tasks.join('\n'))
}

function stripSpaces(line) {
    var i = 0;
    while (i < line.length && (line[i] === ' ' || line[i] === '\t'))
        ++i; //i индекс первого не пробела
    var j = line.length - 1;
    while (j >= 0 && (line[j] === ' ' || line[j] === '\t'))
        --j; // j индекс последнего не пробела
    return line.slice(i, j + 1);
}
// todo пропускать прототипы (пустые строки)
// собирать CarrTypesArr из CSV
function sortPerItem(matrix) {
    var itemData = {};  // id -> {tasks: [], funcs: []}
    for (var i = 0; i < matrix.length; ++i) {
        var id = parseInt(matrix[i][0], 10);
        if (itemData[id] === undefined)
            itemData[id] = { tasks: [], carrtps: [], funcs: [] };
        var task = '';
        if (matrix[i][1] !== '')
            task = matrix[i][1];
        var auType = '';
        if (matrix[i][3] !== '')
            auType = stripSpaces(matrix[i][3]);
        if (matrix[i][4] === '')
            continue; // только заготовка функции
        itemData[id].tasks.push(task);
        itemData[id].carrtps.push(auType);
        itemData[id].funcs.push({
            task: task,
            func: matrix[i].slice(6, 12),
            carrType: matrix[i][5]
        });
    }
    return itemData;
}

function parseJsonFile(auCarrFile) {
    var mapping = {};
    auCarrFile.open('r');
    var content = auCarrFile.read();
    var jsonData = JSON.parse(content);
    /*
    for (var auType in jsonData) {
        var carTypes = [];
        var oneTypeData = jsonData[auType];
        var isObj = !(oneTypeData instanceof Array);
        if (isObj)
            for (var carrType in oneTypeData)
                carTypes.push(carrType);
        mapping[auType] = carTypes;
    }
    */
    auCarrFile.close();
    return jsonData;
}

function collectCarrTypes(jsonData) {
    // jsonData: auType -> {carrType -> [func]}
    var buf = {}; // fID -> carrType
    for (var auType in jsonData) {
        var typeInfo = jsonData[auType];
        for (var carrType in typeInfo) {
            var funcArr = typeInfo[carrType];
            for (var f = 0; f < funcArr.length; ++f) {
                var fID = funcArr[f].split(',')[0];
                buf[fID] = carrType;
            }
        }
    }
    return buf;
}

function importData(doc, itemData) {
    var items = doc.splineItems;
    var tC = 0; var fC = 0;
    var skippedAU = [];
    for (var id in itemData) {
        var idNum = parseInt(id, 10);
        var au = items.itemByID(idNum);
        if (!au.isValid) {
            skippedAU.push(idNum);
            continue;
        }
        var tasks = itemData[id].tasks;
        au.insertLabel(TASKS, tasks.toSource());
        var funcs = itemData[id].funcs; // items of array must have property carrType
        au.insertLabel(FUNCS, funcs.toSource());
        tC += tasks.length;
        fC += funcs.length;
    }
    return [tC, fC];
}

function importCarrTypeHints(doc, au2Carr) {
    var sw = doc.swatches;
    for (var s = 0; s < sw.length; ++s) {
        var swatch = sw[s];
        var auType = swatch.name;
        if (auType in au2Carr) {
            var carrTypeInfo = au2Carr[auType];
            var carrTypes = [];
            for (var carrType in carrTypeInfo)
                carrTypes.push(carrType);
            swatch.insertLabel(CRTPS, carrTypes.toSource());
        }
    }
}

main();
