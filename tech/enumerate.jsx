﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/geometry.jsx');

function mapPageOffsetToFloorCaption(doc) {
    var floorLayer = doc.layers.itemByName('floor');
    if (!floorLayer.isValid) {
        alert('Не найден слой с номерами этажей' +
            '\nСоздайте слой \'floor\'.');
        return undefined;
    }

    var map = {};
    var floorCaptions = floorLayer.textFrames;
    for (var i = 0; i < floorCaptions.length; ++i) {
        var caption = floorCaptions[i];
        var floorCaption = caption.contents;

        var page = caption.parentPage;
        if (page === undefined) {
            alert('Не определена страница для подписи \'' + floorCaption + '\'');
            continue;
        }

        var pageOffset = page.documentOffset;
        if (map[pageOffset] !== undefined)
            alert('На странице ' + page.name + 'обнаружено больше одной подписи\n' +
                'Сохранена \'' + floorCaption + '\'')
        map[pageOffset] = floorCaption;
    }
    return map;
}

function checkMarkerNew(marker) {
    // маркер должен содержать ключ NumberSign - пара значений - первое - документ id, второе его id в SplineItems
    // если ключа нет, значит новый маркер. если есть и неверный, значит копия.
    var lblID = marker.extractLabel('NumberSign');
    if (!lblID || lblID === '')
        return true;
    var id = eval(lblID); // Object{doc, item}
    // marker.parentPage.parent : MasterSpread | Spread
    // return (id.item !== marker.id || 
    //        id.doc  !== marker.parentPage.parent.parent.id);
    return (id.item !== marker.id);
}

function checkPageForNewMarkers(pageInfo, splineItems) {
    var markers = pageInfo.markers;
    for (var m = 0; m < markers.length; ++m) {
        var marker = splineItems.itemByID(markers[m].id);
        if (checkMarkerNew(marker))
            return true;
    }
    return false;
}

function checkPageForDisappearedMarkers(pageInfo, splineItems) {
    // pageInfo : {markers: [{point: 2DPoint, id: Number}], hasNewMarkers: boolean, floorCaption: String}
    if (pageInfo.hasNewMarkers)
        return undefined;
    // проходим по markers берём id, берём сами маркеры
    // по условию hasNewMarkers = false тогда label Number у всех есть, находим хвостовую (по '/') часть
    // отслеживаем их максимум (as number type) и если он будет больше кол-ва -> Disappeared
    var maxNumber = getMaxMarkerNumber(pageInfo, splineItems);

    return (maxNumber > pageInfo.markers.length);
}

function getMaxMarkerNumber(pageInfo, splineItems) {
    var markers = pageInfo.markers;
    var maxNumber = 0;
    for (var m = 0; m < markers.length; ++m) {
        var marker = splineItems.itemByID(markers[m].id);
        var numberLabel = marker.extractLabel('Number').split('/');
        var number = Number(numberLabel.pop());
        var floorCaption = numberLabel.pop();
        var layerName = numberLabel.pop();

        if (number > maxNumber
            && floorCaption == pageInfo.floorCaption
            && layerName == app.activeDocument.activeLayer.name)
            maxNumber = number;
    }

    return maxNumber;
}

function extractNumber(marker) {
    var lblNumber = marker.extractLabel('Number');
    if (lblNumber && lblNumber != '') {
        var numb = Number(lblNumber.split('/').pop());
        if (!isNaN(numb))
            return numb;
    }
    else
        return undefined;
}

function checkNumbersPresence(data, splineItems) {
    for (var pageOffset in data) {
        var pageInfo = data[pageOffset];
        var markers = pageInfo.markers;
        for (var m = 0; m < markers.length; ++m) {
            var marker = splineItems.itemByID(markers[m].id);
            if (extractNumber(marker) !== undefined)
                return true;
        }
    }
    return false;
}

function collectMarkerPlacements(markers, mapOffsetToFloor, colorName) {
    var data = {};
    for (var i = 0; i < markers.length; ++i) {
        var marker = markers[i];
        var page = marker.parentPage;
        var pageOffset = page.documentOffset;
        var pageName = page.name;

        if (colorName !== undefined) {
            marker.insertLabel('ExpectingColorName', colorName);
            if (marker.fillColor.name !== colorName) {  // exclide from numbering process and remove old label
                marker.insertLabel('Number', '');
                continue;
            }
        }

        if (!data[pageOffset]) data[pageOffset] = { markers: [], order: [], name: pageName };
        data[pageOffset].markers.push({
            point: takeMostLeft(marker),
            id: marker.id,
            taken: false
        });
    }

    for (var pageOffset in data) {
        data[pageOffset].hasNewMarkers = checkPageForNewMarkers(data[pageOffset], markers);
        data[pageOffset].hasDisappeareds = checkPageForDisappearedMarkers(data[pageOffset], markers);

        var floorLabel = mapOffsetToFloor[pageOffset];
        if (floorLabel !== undefined)
            data[pageOffset].floorCaption = floorLabel;
    }

    // data: pageOffset -> {markers: [{point: 2DPoint, id: Number}], hasNewMarkers: boolean, floorCaption: String}
    return data;
}

function isNaN(val) { return Number(val) !== Number(val); }

function extractNumberingRound(layer) {
    var lblNumberingRound = layer.extractLabel('NumberingRound');
    if (!lblNumberingRound || lblNumberingRound === '' || isNaN(lblNumberingRound))
        return undefined;
    else
        return Number(lblNumberingRound);
}

function evalNumberingRound(layer, data) {
    // берём из label NumberingRound если там число, иначе
    // если нумерация ещё не проводилась считаем, что 0 иначе 1 и устанавливаем этот label
    var numbRound = extractNumberingRound(layer);
    if (!numbRound) {
        var numbRound = (checkNumbersPresence(data, layer.splineItems) ? 1 : 0);
        layer.insertLabel('NumberingRound', String(numbRound));
    }

    return numbRound;
}

function incrementNumberingRound(layer) {
    var numbRound = extractNumberingRound(layer);
    if (!numbRound)
        numbRound = -1;

    layer.insertLabel('NumberingRound', String(numbRound + 1));
}

function checkFloorCaptions(data) {
    var pageNames = [];
    for (var pageOffset in data) {
        var pageInfo = data[pageOffset];
        if (pageInfo.floorCaption === undefined)
            pageNames.push(pageInfo.name);
    }

    if (pageNames.length > 0) {
        alert('Не найдена подпись этажа на страницах:\n' + pageNames.join(', '));
        return false;
    }
    else
        return true;
}

function genSign(marker) {
    // marker.parentPage.parent : MasterSpread | Spread
    var sign = {
        doc: marker.parentPage.parent.parent.id,
        item: marker.id
    };
    return sign.toSource();
}
function genSignMoving(marker) {
    // marker.parentPage.parent : MasterSpread | Spread
    var sign = {
        // doc:    marker.parentPage.parent.parent.id,
        item: marker.id
    };
    return sign.toSource();
}

function resetNumberLabels(pageInfo, layerName, splineItems, numbRound) {
    // pageInfo: {floorCaption: String, markers: [{point: 2DPoint, id: Number, taken: boolean}], order: [перестановка на markers]}
    var floorCaption = pageInfo.floorCaption;
    function genNumber(n) { return [layerName, floorCaption, n].join('/'); }

    var markers = pageInfo.markers;
    var order = pageInfo.order;
    var countNum = getMaxMarkerNumber(pageInfo, splineItems);

    var idNeedNum = [];
    for (var o = 0; o < order.length; ++o) {
        var markerId = markers[order[o]].id;
        var marker = splineItems.itemByID(markerId);
        if (checkMarkerNew(marker)) {
            idNeedNum.push(o);
        }
    }

    for (var i = 0; i < idNeedNum.length; ++i) {
        var o = idNeedNum[i];
        var markerId = markers[order[o]].id;
        var marker = splineItems.itemByID(markerId);
        marker.insertLabel('Number', genNumber(countNum + i + 1));
        marker.insertLabel('NumberSign', genSignMoving(marker));
        marker.insertLabel('NumberingRound', String(numbRound));
    }
}
function resetAUNumberLabels(pageInfo, layerName, splineItems) {
    // pageInfo: {floorCaption: String, markers: [{point: 2DPoint, id: Number, taken: boolean}], order: [перестановка на markers]}
    var floorCaption = pageInfo.floorCaption;
    function genNumber(n, marker) { return [n, marker.fillColor.name].join('/'); }
    function genSign(marker) {
        // marker.parentPage.parent : MasterSpread | Spread
        var sign = {
            doc: marker.parentPage.parent.parent.id,
            item: marker.id
        };
        return sign.toSource();
    }
    var markers = pageInfo.markers;
    var order = pageInfo.order;
    for (var o = 0; o < order.length; ++o) {
        var markerId = markers[order[o]].id;
        var marker = splineItems.itemByID(markerId);
        marker.insertLabel('Number', genNumber(o + 1, marker));
        marker.insertLabel('NumberSign', genSign(marker));
    }
}

function enumerate(doc) {
    var layer = doc.activeLayer;
    var layerName = layer.name;
    var markers = layer.splineItems;

    var mapOffsetToFloor = mapPageOffsetToFloorCaption(doc);
    if (!mapOffsetToFloor)
        return false;

    var data = collectMarkerPlacements(markers, mapOffsetToFloor, layer.name);
    if (!checkFloorCaptions(data))
        return false;
    // data: pageOffset -> {markers: [{point: 2DPoint, id: Number}], hasNewMarkers: boolean, floorCaption: String, name: String}
    var numbRound = evalNumberingRound(layer, data);

    var offsetsToUpdate = [];
    for (var pageOffset in data)
        if (data[pageOffset].hasNewMarkers || data[pageOffset].hasDisappeareds)
            offsetsToUpdate.push(pageOffset);

    if (offsetsToUpdate.length > 0) {
        var c = confirm('На слое \'' + layerName + '\' требуется обновление нумерации.\n' +
            'Запустить перенумерацию?');
        if (!c) {
            alert('Отменено.');
            return false;
        }
        else {
            var pageNames = [];
            for (var o = 0; o < offsetsToUpdate.length; ++o) {
                var pageOffset = offsetsToUpdate[o];
                var pageInfo = data[pageOffset];
                defineMarkersOrder(pageInfo);
                // data: pageOffset -> {markers: [{point: 2DPoint, id: Number}], hasNewMarkers: boolean, 
                //                      floorCaption: String, name: String, order: [перестановка на markers]}
                resetNumberLabels(pageInfo, layerName, markers, numbRound);
                pageNames.push(pageInfo.name);
            }

            incrementNumberingRound(layer);
            alert('Нумерация объектов \'' + layerName + '\' обновлена.\n' +
                'Изменения на страницах: ' + pageNames.join(', '));
            return true;
        }
    }
    else
        return true;
}

function enumerateAU(doc) {
    var layer = doc.activeLayer;
    var layerName = layer.name;
    var markers = layer.splineItems;

    var mapOffsetToFloor = mapPageOffsetToFloorCaption(doc);
    if (!mapOffsetToFloor)
        return false;

    var data = collectMarkerPlacements(markers, mapOffsetToFloor);
    if (!checkFloorCaptions(data))
        return false;
    // data: pageOffset -> {markers: [{point: 2DPoint, id: Number}], hasNewMarkers: boolean, floorCaption: String, name: String}

    var offsetsToUpdate = [];
    for (var pageOffset in data)
        if (data[pageOffset].hasNewMarkers || data[pageOffset].hasDisappeareds)
            offsetsToUpdate.push(pageOffset);

    if (offsetsToUpdate.length > 0) {
        var c = confirm('На слое \'' + layerName + '\' требуется обновление нумерации.\n' +
            'Запустить перенумерацию?');
        if (!c) {
            alert('Отменено.');
            return false;
        }
        else {
            var pageNames = [];
            for (var o = 0; o < offsetsToUpdate.length; ++o) {
                var pageOffset = offsetsToUpdate[o];
                var pageInfo = data[pageOffset];
                defineMarkersOrder(pageInfo);
                // data: pageOffset -> {markers: [{point: 2DPoint, id: Number}], hasNewMarkers: boolean, 
                //                      floorCaption: String, name: String, order: [перестановка на markers]}
                resetAUNumberLabels(pageInfo, layerName, markers);
                pageNames.push(pageInfo.name);
            }

            alert('Нумерация на слое \'' + layerName + '\' обновлена.\n' +
                'Изменения на страницах: ' + pageNames.join(', '));
            return true;
        }
    }
    else
        return true;
}
function enumerateCarefull(doc) {
    var layer = doc.activeLayer;
    var layerName = layer.name;
    var markers = layer.splineItems;

    var mapOffsetToFloor = mapPageOffsetToFloorCaption(doc);
    if (!mapOffsetToFloor)
        return false;

    var data = collectMarkerPlacements(markers, mapOffsetToFloor);
    if (!checkFloorCaptions(data))
        return false;
    // data: pageOffset -> {markers: [{point: 2DPoint, id: Number}], hasNewMarkers: boolean, floorCaption: String, name: String}

    var offsetsToUpdate = [];
    //for (var pageOffset in data)
    //    if (data[pageOffset].hasNewMarkers || data[pageOffset].hasDisappeareds)
    //        offsetsToUpdate.push(pageOffset);

    if (offsetsToUpdate.length > 0) {
        var c = confirm('На слое \'' + layerName + '\' требуется обновление нумерации.\n' +
            'Запустить перенумерацию?');
        if (!c) {
            alert('Отменено.');
            return false;
        }
        else {
            var pageNames = [];
            for (var o = 0; o < offsetsToUpdate.length; ++o) {
                var pageOffset = offsetsToUpdate[o];
                var pageInfo = data[pageOffset];
                defineMarkersOrder(pageInfo);
                // data: pageOffset -> {markers: [{point: 2DPoint, id: Number}], hasNewMarkers: boolean, 
                //                      floorCaption: String, name: String, order: [перестановка на markers]}
                resetNumberLabels(pageInfo, layerName, markers);
                pageNames.push(pageInfo.name);
            }

            alert('Нумерация на слое \'' + layerName + '\' обновлена.\n' +
                'Изменения на страницах: ' + pageNames.join(', '));
            return true;
        }
    }
    else
        return true;
}

function takeMarkerNumber(marker) {
    var lblNumber = marker.extractLabel('Number');

    var lblExpColorname = marker.extractLabel('ExpectingColorName');
    if (lblExpColorname !== undefined && lblExpColorname !== '') {
        if (marker.fillColor.name !== lblExpColorname)
            lblNumber = '!!! ' + lblNumber;
    }
    return lblNumber;
}
