﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');

var masters = '/Мастер-файлы';
var templates = '/Шаблоны';

function createDir_alert(dir) {
    dir = (dir instanceof Folder) ? dir : new Folder(dir);
    var created = dir.create();
    if (!created)
        alert('Не удаётся создать папку\n' + dir.fullName);
    return created;
}

function createCasesDoc(rootDir, templateName) {
    var templatesDir = new File(rootDir + templates);
    if (!templatesDir.exists && !createDir_alert(templatesDir))
        return undefined;

    var templateFile = new File(templatesDir + '/' + templateName + '.indd');
    var casesFile = new File(templatesDir + '/' + templateName + '_cases' + '.indd');

    if (!templateFile.exists) {
        alert('Создайте файл шаблона!\n' + templateFile.fullName);
        return undefined;
    }
    var templateDoc = app.open(templateFile);
    templateDoc.saveACopy(casesFile);
    templateDoc.close();
    return app.open(casesFile);
}

function getCasesDoc(rootDir, templateName) {
    var templatesDir = new File(rootDir + templates);
    var casesFile = new File(templatesDir + '/' + templateName + '_cases' + '.indd');
    if (casesFile.exists)
        return app.open(casesFile);
    else
        return undefined;
}

function getTemplateFile(rootDir, templateName) {
    var templatesDir = new File(rootDir + templates);
    var templateFile = new File(templatesDir + '/' + templateName + '.indd');
    return templateFile;
}

// это для генерации файла со случаями использования шаблона
//==========================================================
// а это для превьюшек

function getPreviewDoc(docIn, shape, layerName) {
    var mfCurTypeDirPath = getMasterFilePath(docIn);
    // var mfCurTypeDirPath = 'C:\\Users\\2\\Downloads\\z_scripts_test_1803\\';
    if (mfCurTypeDirPath === undefined)
        return undefined;

    if (!layerName)
        layerName = docIn.activeLayer.name;
    var namePrefix = docIn.name.split('_').slice(0, 2).join('_') + '_';
    // var namePrefix = '';

    var mfPath = mfCurTypeDirPath + namePrefix + layerName + '.indd';
    var previewPath = mfCurTypeDirPath + namePrefix + layerName + '_preview' + getDatePostfix() + '.indd';

    var previewFile = new File(previewPath);
    var masterFile = File(mfPath);
    if (!masterFile.exists) {
        var mfDoc = fillInMasterFile(masterFile, shape);
        alert('Мастер-файл не найден по адресу \'' + mfPath +
            '\'.\nСоздан мастер-файл стандартного наполнения.');
    }
    else
        var mfDoc = app.open(masterFile);
    mfDoc.saveACopy(previewFile);
    return app.open(previewFile);
}

function fillInMasterFile(masterFile, shape) {
    var pageWidth = 1000;
    var pageHeight = 1000;

    var mfDoc = app.documents.add();
    var page = mfDoc.pages[0];
    var dpr = mfDoc.documentPreferences;
    dpr.pageWidth = (pageWidth + 'mm');
    dpr.pageHeight = (pageHeight + 'mm');
    dpr.facingPages = false;

    for (var f = 0; f < shape; f++) {
        var top = f * Math.round(pageWidth / shape) + 10;
        var bottom = (f + 1) * Math.round(pageWidth / shape) - 10;
        var field = page.textFrames.add(mfDoc.layers[0], LocationOptions.AT_END);
        field.geometricBounds = [top + 'mm', 10 + 'mm', bottom + 'mm', (pageHeight - 10) + 'mm'];
        field.label = '%%' + (f + 1);
    }

    var numLayer = mfDoc.layers.add({ name: 'numbers' });
    page.textFrames.add({
        itemLayer: numLayer,
        geometricBounds: ['10mm', '10mm', '40mm', '60mm'],
        label: '%%num',
    });

    mfDoc.save(masterFile);
    return mfDoc;
}

function getMasterFilePath(docIn) {
    var rootDir = docIn.filePath.parent.fullName;

    var mfDirPath = rootDir + masters;
    var mfDir = Folder(mfDirPath);
    if (!mfDir.exists && !createDir_alert(mfDir))
        return undefined;

    function replaceSpacesNDots(path) {
        return path.replace(' ', '_').replace('.', '_');
    }
    var layerName = replaceSpacesNDots(docIn.activeLayer.name);
    var mfCurTypeDirPath = mfDirPath + '/' + layerName + '/';
    var mfCurTypeDir = Folder(mfCurTypeDirPath);
    if (!mfCurTypeDir.exists) {
        if (createDir_alert(mfCurTypeDir))
            alert('Создана папка мастер-файлов для типа ' + layerName +
                '\nРасположение: \'' + mfCurTypeDir.fullName + '\'');
        else
            return undefined;
    }
    return mfCurTypeDirPath;
}

function getCSV(csvPath, type) {
    var csvFile = new File(csvPath);
    if (!csvFile.exists) {
        var alertText;
        if (type === 'picts')
            alertText = 'Создан пустой словарь замены пиктограмм';
        else
            alertText = 'Создан пустой словарь замены имен poi';

        // csvFile.open('w');
        writeFile(csvFile, '', 'UTF-8');
        // csvFile.close();
        alert(alertText + ' \n\'' + csvFile.fullName +
            '\'.');
        return undefined;
    }
    return csvFile;
}
