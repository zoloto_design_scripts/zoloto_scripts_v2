﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/files.jsx');

var rootDir;

function generateOneTemplateCases(tName, cases) {
    var curDoc = createCasesDoc(rootDir, tName);
    if (!curDoc)
        return;

    var mainPage = curDoc.pages.firstItem();
    var index = {};
    for (var cas in cases) {
        if (cas === 'cases')
            continue;
        var args = cas.slice(1, cas.length - 1).split(','); // NB: аргументы нумеруются 0, 1... , а в шаблоне %1, %2, ....
        var page = mainPage.duplicate(LocationOptions.AT_END);

        var tFrames = page.textFrames;
        for (var f = 0; f < tFrames.length; ++f) {
            var frame = tFrames[f];
            if (frame.label === 'main')
                var main = frame;

            var pattern = frame.contents;
            for (var i = args.length - 1; i >= 0; --i) // в обратном порядке, чтобы %10 обрабатывалось до %1
                pattern = pattern.replace('%' + (i + 1), args[i]);
            frame.contents = pattern;
        }
        if (!main) {
            alert('Не найден TextFrame с label \'main\'');
            return;
        }
        index[cas] = main.id;
    }
    curDoc.insertLabel('index', index.toSource());
    mainPage.remove();
    curDoc.save();
    return curDoc;
}

// проверяет наличие всех нужных случаев
function checkCasesPresence(casesNeed, casesGot) { // casesNeed = args->nTh; casesGot = args -> textFrame's ID
    for (var args in casesNeed) {
        if (args !== 'cases' && !casesGot[args])
            return false;
    }
    return true;
}

function checkMasterUpdates(templateCasesFile, templateFile) {
    var masterModified = templateFile.modified.getTime();
    var casesModified = templateCasesFile.modified.getTime();
    return (masterModified > casesModified);
}
