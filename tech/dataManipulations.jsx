﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/geometry.jsx');

function collectData(selection) {
    var shape = 0;
    var nVars = {};
    for (var i = 0; i < selection.length; ++i) {
        var mark = selection[i];
        var label = checkMess(mark.label).text;
        var vars = label.split(';\r');
        vars.number = mark.extractLabel('Number');
        vars.id = mark.id;
        data.push(vars);

        var len = vars.length;
        if (len > shape)
            shape = len;
        if (nVars[len] === undefined)
            nVars[len] = 0;
        ++nVars[len];
    }
    data.shape = shape;
    data.statistics = nVars;
}

function collectDataCsv(selection) {
    var shape = 0;
    var nVars = {};

    for (var i = 0; i < selection.length; ++i) {
        var mark = selection[i];
        var label = checkMess(mark.label).text;
        var vars = [mark.id].concat(label.split(';\r'));
        vars.push(mark.extractLabel('Number'), mark.parentPage.documentOffset + 1);
        var pos = takeMostLeft(mark);
        vars.push(pos[0], pos[1]);

        data.push(vars);

        var len = vars.length;
        if (len > shape)
            shape = len;
        if (nVars[len] === undefined)
            nVars[len] = 0;
        ++nVars[len];
    }
    data.shape = shape;
    data.statistics = nVars;
}

function collectDataCsvBrowser(selection) {
    var shape = 0;
    var nVars = {};

    for (var i = 0; i < selection.length; ++i) {
        var mark = selection[i];
        var lblNumber = mark.extractLabel('Number');
        if (lblNumber === undefined || lblNumber === '')
            continue;   // не выружаем если нет номера

        var label = checkMess(mark.label).text;
        var vars = [lblNumber].concat(label.split(';\r'));
        var pathStr = pathToString(mark);
        vars.push(pathStr);
        data.push(vars);

        var len = vars.length;
        if (len > shape)
            shape = len;
        if (nVars[len] === undefined)
            nVars[len] = 0;
        ++nVars[len];
    }
    data.shape = shape;
    data.statistics = nVars;
}

function collectDataCsvNoStaff(selection) {
    var shape = 0;
    var nVars = {};

    for (var i = 0; i < selection.length; ++i) {
        var mark = selection[i];
        var lblNumber = mark.extractLabel('Number');
        if (lblNumber === undefined || lblNumber === '')
            continue;   // не выружаем если нет номера

        var label = checkMess(mark.label).text;
        var vars = [lblNumber].concat(label.split(';\r'));
        vars.push(mark.id);
        data.push(vars);

        var len = vars.length;
        if (len > shape)
            shape = len;
        if (nVars[len] === undefined)
            nVars[len] = 0;
        ++nVars[len];
    }
    data.shape = shape;
    data.statistics = nVars;
}

function compNumberingLabels(numA, numB) {
    // expected labels form: TYPE_NAME/flourname/number
    // returns less than 0 if a is less than b (and so on)
    numA = String(numA);
    if (numA === '')
        return 0;
    var partsA = numA.split('/');
    if (partsA.length !== 3) {
        // throw new Error('expected label format TYPE_NAME/flourname/number, got: ' + numA);
        alert('expected label format TYPE_NAME/flourname/number, got: \"' + numA + '\"');
        return 0;
    }

    numB = String(numB);
    if (numB === '')
        return 0;
    var partsB = numB.split('/');
    if (partsB.length !== 3) {
        // throw new Error('expected label format TYPE_NAME/flourname/number, got: ' + numB);
        alert('expected label format TYPE_NAME/flourname/number, got: \"' + numB + '\"');
        return 0;
    }

    var typeA = partsA[0];
    var typeB = partsB[0];
    if (typeA !== typeB) {
        if (typeA > typeB)
            return 1;
        else if (typeA < typeB)
            return -1;
        else
            return 0;
    }

    var flA = partsA[1];
    var flB = partsB[1];
    if (flA !== flB) {
        if (flA > flB)
            return 1;
        else if (flA < flB)
            return -1;
        else
            return 0;
    }

    var nA = Number(partsA[2]);
    if (nA !== nA) { // check for NaN
        // throw new Error('last part of label expected to be number, got: ' + numA);
        alert('last part of label expected to be number, got: ' + numA);
        return 0;
    }

    var nB = Number(partsB[2]);
    if (nB !== nB) { // check for NaN
        // throw new Error('last part of label expected to be number, got: ' + numB);
        alert('last part of label expected to be number, got: ' + numB);
        return 0;
    }

    if (nA > nB)
        return 1;
    else if (nA < nB)
        return -1;
    else
        return 0;
}

function sortMatrix(matr, nKeyColumn, comp) {
    function compRows(rowA, rowB) {
        // return comp(rowA[rowA.length - endNKeyColumn - 1], rowB[rowB.length - endNKeyColumn - 1]);
        return comp(rowA[nKeyColumn], rowB[nKeyColumn]);
    }

    matr.sort(compRows);    // sort in-place
}

function generateHeader(layer, nVars, staffNames) {
    var prevVariableFormat = [];
    var lblVariableFormat = layer.extractLabel('VarsNames');
    if (lblVariableFormat && lblVariableFormat !== '')
        prevVariableFormat = eval(lblVariableFormat);
    if (prevVariableFormat.length === 0)
        prevVariableFormat = new Array(nVars);

    var header = ['number'].concat(prevVariableFormat);
    if (staffNames === undefined)
        header.push('id', 'page', 'posX', 'posY');
    else
        header.push(staffNames);
    return header;
}

/**
    Формат представления CSV-файла:
        словарь с ключом, который берётся из первого столбца каждой строки
        значение словаря - массивы с доп функцией, все фиксированной длины
        равной максимуму по строкам (если где-то меньше - добиваем пустыми)
        первая строка образует соответствие имя_колонки-номер_колонки, которое хранится в __meta.keys
        это соответствие используется для доступа в массиве переменных через функцию withKey()
    !!! При итерации по полученному объекту будет встречено служебное свойство _meta.
*/

function parseCSV(dictFile) {
    // доступ к файлу и считывание
    dictFile.open('r');
    var buf = dictFile.read().split('\n');

    var result = {};
    result.__meta = {};
    // разбор заголовка
    var header = buf[0].split(',');
    var indices = {};
    result.__meta.header = header;
    for (var i = 0; i < header.length; ++i)
        indices[header[i]] = i;
    result.__meta.keys = indices;

    function fabricOfMethod_withKey(cols) {
        return function (key) {
            if (!indices[key])
                return undefined;
            else
                return (indices[key] > 0
                    ? cols[indices[key] - 1] : key);
        }
    }
    // разбор файла
    var shape = 0;
    for (var l in buf) {
        var line = buf[l];
        if (line.length === 0)
            continue;

        var parts = parseCsvLine(line);
        var key = parts.shift();
        result[key] = parts;
        result[key].withKey = fabricOfMethod_withKey(parts);

        if (parts.length > shape)
            shape = parts.length;
    }
    result.__meta.shape = shape;

    // выравнивание переменных
    for (var k in result) {
        if (k === '__meta') continue;
        var entry = result[k];
        while (entry.length < shape)
            entry.push('');
    }

    dictFile.close();
    return result;
}

function parseCsvLine(line) {
    function parseCell(line, start) {
        if (start > line.length)
            throw new Error("out of bounds");
        if (start >= line.length - 1)
            return line.length;
        if (line[start] !== '"') {
            var next = line.indexOf(',', start);
            return (next === -1 ? line.length : next);
        }

        // parse escaped cell
        var br = start;
        var found = false;
        while (!found) {
            br = line.indexOf('"', br + 1);
            if (br === -1)
                throw new Error("invalid escape");
            if (line[br - 1] !== '\\')
                found = true;
        }
        return br + 1;
    }
    function unescape(cell) {
        if (cell[0] === '"' && cell[cell.length - 1] === '"')
            return cell.substring(1, cell.length - 1).replace('\\"', '"');
        else
            return cell;
    }

    var parts = [];
    var start = 0;
    var end = parseCell(line, start);
    while (end !== line.length) {
        parts.push(unescape(line.substring(start, end)));
        start = end + 1;
        end = parseCell(line, start);
    }
    parts.push(unescape(line.substring(start, end)));
    return parts;
}

function deduceTailStuffCount(header, defaultCount) {
    // assuming that "stuff" vars begin at attr "id"
    var numberPlacement = 0;    // last "id" in header array
    for (var i = 0; i < header.length; ++i) {
        if (header[i] === '\"id\"')
            numberPlacement = i;
    }

    if (numberPlacement === 0)
        return defaultCount;
    return (header.length - numberPlacement);   // count stuff including "number" itself
}

function parseCsvOnlyData(dictFile) {
    // доступ к файлу и считывание
    dictFile.open('r');
    var buf = dictFile.read().split('\n');

    var result = {};    // old variant
    var arrayBuf = [];
    result.__meta = {};
    // разбор заголовка
    var header = buf[0].split(',');
    var indices = {};
    result.__meta.header = header;
    for (var i = 0; i < header.length; ++i)
        indices[header[i]] = i;
    result.__meta.keys = indices;

    function fabricOfMethod_withKey(cols) {
        return function (key) {
            if (!indices[key])
                return undefined;
            else
                return (indices[key] > 0
                    ? cols[indices[key] - 1] : key);
        }
    }
    var rightStuffVarsCount = deduceTailStuffCount(header, 4);  // потому что раньше последние 4 переменных были служебные

    // разбор файла
    var shape = 0;
    for (var l in buf) {
        var line = buf[l];
        if (line.length === 0)
            continue;

        var parts = parseCsvLine(line);
        // var key = parts.shift();
        // var vars = parts.slice(0, parts.length - rightStuffVarsCount);
        var key = parts.pop();
        var vars = parts.slice(1, parts.length);
        result[key] = vars;
        arrayBuf.push(vars);

        result[key].withKey = fabricOfMethod_withKey(parts);

        if (vars.length > shape)
            shape = parts.length;
    }
    result.__meta.shape = shape;

    // выравнивание переменных
    for (var k in result) {
        if (k === '__meta') continue;
        var entry = result[k];
        while (entry.length < shape)
            entry.push('');
    }

    dictFile.close();
    return result;
}

function replaceAll(where, oldV, newV) {
    for (var i = 0; i < where.length; ++i)
        for (var j = 0; j < where[i].length; ++j)
            while (where[i][j].indexOf(oldV) !== -1)
                where[i][j] = where[i][j].replace(oldV, newV);
}

function replaceByMap(data, map) {
    for (var code in map)
        if (code !== '__meta')
            replaceAll(data, code, map[code][0]);
}

function replaceByMultimap(data, mmap) {
    // список возможных суффиксов есть все известные ключи столбцов
    for (var suf in mmap.__meta.keys)
        for (var code in mmap)
            if (code !== '__meta')
                replaceAll(data, code + '_' + suf, mmap[code].withKey(suf));

    //теперь случаи использования без суффикса
    for (var code in mmap)
        if (code !== '__meta')
            replaceAll(data, code, mmap[code][0]);
}