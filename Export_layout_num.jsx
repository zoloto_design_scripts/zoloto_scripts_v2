﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/files.jsx');

var docIn;
var folderName;
var folder;
var needUndo = false;

app.doScript(main, ScriptLanguage.JAVASCRIPT, undefined,
    UndoModes.entireScript, "ExportLayoutNum");

if (needUndo) app.undo();

function main() {
    docIn = app.activeDocument;
    var mainFolderPrefix = docIn.name.slice(0, docIn.name.indexOf("preview"));
    var docDir = docIn.filePath;

    var input = collectInput();
    if (!input) return;

    folderName = String(docDir)+ "/" + getFolderName(input, mainFolderPrefix);
    folder = Folder(folderName);
    if (!folder.exists && !createDir_alert(folder)) return;

    switch (input) {
        case "jpg 150":
            setJPG150ExportProperties();
            break;
        case "jpg 72":
            setJPG72ExportProperties();
            break;
        case "eps":
            setEPSExportProperties();
            break;
        case "pdf":
            setPDFExportProperties();
            break;
    }
}

function collectInput() {
    var dialogWindow = new Window("dialog", "Экспорт страниц");
    dialogWindow.preferredSize = [372,100];
    dialogWindow.alignChildren = "right";

    var choosePanel = dialogWindow.add("panel", undefined, "Формат");
    choosePanel.orientation = "column";
    choosePanel.alignChildren = "right";
    choosePanel.margins = 20;
    preferredSize = [340,50];

    var formatDropdown = choosePanel.add("dropdownlist", undefined, ["jpg 150", "jpg 72", "eps", "pdf"]);
    formatDropdown.selection = 0;
    formatDropdown.preferredSize = [300,21];

    var buttonGroup = dialogWindow.add("group");
    var cancelBtn = buttonGroup.add ("button", undefined, "Cancel");
    var exportBtn = buttonGroup.add ("button", undefined, "Export", {name:"ok"});

    exportBtn.onClick = function() { exit(); }
    if (dialogWindow.show() == 1) {
        return formatDropdown.selection.text;
    } else {
        return;
    }
}

function getFolderName(input, prefix) {
    var name;
    switch (input) {
        case "jpg 150":
            name = prefix + "jpg150" + getDatePostfix();
            break;
        case "jpg 72":
            name = prefix + "jpg72" + getDatePostfix();
            break;
        case "eps":
            name = prefix + "eps" + getDatePostfix();
            break;
        case "pdf":
            name = prefix + "pdf" + getDatePostfix();
            break;
    }

    return name;
}

function getFloorName(page) {
    var tfs = docIn.layers.itemByName("number_layer").textFrames;
    for (var i = 0; i < tfs.length; i++) {
        if (tfs[i].parentPage == page) return tfs[i].contents;
    }
}

function setJPG150ExportProperties() {
    app.jpegExportPreferences.jpegQuality = JPEGOptionsQuality.MAXIMUM;
    app.jpegExportPreferences.jpegRenderingStyle = JPEGOptionsFormat.BASELINE_ENCODING;
    app.jpegExportPreferences.jpegColorSpace = JpegColorSpaceEnum.RGB;
    app.jpegExportPreferences.embedColorProfile = true;
    app.jpegExportPreferences.antiAlias = true;
    app.jpegExportPreferences.useDocumentBleeds = false;
    app.jpegExportPreferences.simulateOverprint = false;
    app.jpegExportPreferences.exportResolution = 150;
    app.jpegExportPreferences.jpegExportRange = ExportRangeOrAllPages.EXPORT_RANGE;

    var endPosition = docIn.pages.lastItem().documentOffset;

    for (var i = 0; i <= endPosition; i++) {
        var page = docIn.pages.item(i);
        var floorName = getFloorName(page);
        var fileName = floorName.replace(/\//g, "_");
        app.jpegExportPreferences.pageString = String(i + 1);

        docIn.exportFile(ExportFormat.JPG, File(folderName + "/" + fileName + ".jpg"), false);
    }
}

function setJPG72ExportProperties() {
    app.jpegExportPreferences.jpegQuality = JPEGOptionsQuality.MAXIMUM;
    app.jpegExportPreferences.jpegRenderingStyle = JPEGOptionsFormat.BASELINE_ENCODING;
    app.jpegExportPreferences.jpegColorSpace = JpegColorSpaceEnum.RGB;
    app.jpegExportPreferences.embedColorProfile = true;
    app.jpegExportPreferences.antiAlias = true;
    app.jpegExportPreferences.useDocumentBleeds = false;
    app.jpegExportPreferences.simulateOverprint = false;
    app.jpegExportPreferences.exportResolution = 72;
    app.jpegExportPreferences.jpegExportRange = ExportRangeOrAllPages.EXPORT_RANGE;

    var endPosition = docIn.pages.lastItem().documentOffset;

    for (var i = 0; i <= endPosition; i++) {
        var page = docIn.pages.item(i);
        var floorName = getFloorName(page);
        var fileName = floorName.replace(/\//g, "_");
        app.jpegExportPreferences.pageString = String(i + 1);


        docIn.exportFile(ExportFormat.JPG, File(folder + "/" + fileName + ".jpg"), false);
    }
}

function outlineAll() {
    var textFrames = docIn.textFrames.everyItem().getElements();

    for (var i = 0; i < textFrames.length; i++) {
        try {
            if (textFrames[i].contents.lastIndexOf("%%") == 0) continue;
            textFrames[i].createOutlines();
        }
        catch (e) { }
    }
}

function offsetsToFilenames() {
    var endPosition = docIn.pages.lastItem().documentOffset;
    var result = {};

    for (var i = 0; i <= endPosition; i++) {
        var page = docIn.pages.item(i);
        var name = getFloorName(page);
        var fileName = name.replace(/\//g, "_");

        result[page.documentOffset] = fileName;
    }

    return result;
}

function setEPSExportProperties() {
    needUndo = true;
    var fileNames = offsetsToFilenames();

    var endPosition = docIn.pages.lastItem().documentOffset;
    outlineAll();

    for (var i = 0; i <= endPosition; i++) {
        var page = docIn.pages.item(i);
        app.epsExportPreferences.pageRange = String(i + 1);

        docIn.exportFile(ExportFormat.EPS_TYPE, File(folder + "/" + fileNames[page.documentOffset] + ".eps"), false);
    }
}

function setPDFExportProperties() {
    needUndo = true;
    var fileNames = offsetsToFilenames();

    var endPosition = docIn.pages.lastItem().documentOffset;
    outlineAll();

    for (var i = 0; i <= endPosition; i++) {
        var page = docIn.pages.item(i);
        app.pdfExportPreferences.pageRange = String(i + 1);

        docIn.exportFile(ExportFormat.PDF_TYPE, File(folder + "/" + fileNames[page.documentOffset] + ".pdf"), false);
    }
}
