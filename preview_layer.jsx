﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/checkMess.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/dataManipulations.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/files.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/templateCases.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/generationHelpers.jsx');

var data = [];
var namePrefix;
var rootDir;
var docDir;
var placement = '/Словари';

var specialChars = {
    '&br': SpecialCharacters.FORCED_LINE_BREAK,
    '&tab': '\t'
};

app.doScript(main, undefined, undefined,
    UndoModes.entireScript, "PreviewLayer");

function main() {
    var docIn = app.activeDocument;
    namePrefix = docIn.name.split('_')[0] + '_' + docIn.name.split('_')[1] + '_';
    docDir = docIn.filePath;
    rootDir = docIn.filePath.parent;
    var selection = docIn.activeLayer.splineItems;
    collectData(selection);

    var previewDoc = getPreviewDoc(docIn, data.shape);
    if (!previewDoc) {
        alert('Проблемы при создании файла для вывода превью.\n' +
            'Вероятно не удалось создать папки мастер-файлов.');
        return;
    }

    replacePictsNames();
    var templatesUsages = collectTemplatesUsages();
    var templatesCasesDocs = getCasesDocActualized(templatesUsages);
    var p = generatePreviews(previewDoc, templatesCasesDocs);
    closeAllTemplateDocs(templatesCasesDocs);
    alert('Превью созданы и лежат в папке\n\''
        + previewDoc.fullName + '\'');
}
