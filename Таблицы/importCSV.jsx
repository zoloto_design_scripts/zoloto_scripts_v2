﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/dataManipulations.jsx');

app.doScript(main, undefined, undefined,
    UndoModes.entireScript, "ImportCSV");

function main() {
    var docIn = app.activeDocument;
    var layers = docIn.layers;

    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, layers.length);
    progress.maxvalue = layers.length;
    progress.value = 0;

    var c = 0;
    var ch = 0;
    for (var l = 0; l < layers.length; ++l) {
        progress.value++;
        var L = layers[l];
        if (!L.visible)
            continue;

        var csvPath = getCsvPath(docIn, L.name);
        var csvFile = new File(csvPath);
        var csvData = parseCsvOnlyData(csvFile);
        var markers = L.splineItems;
        for (var m = 0; m < markers.length; ++m) {
            var mark = markers[m];
            if (csvData.hasOwnProperty(mark.id)) {
                var vars = csvData[mark.id];
                var varsToScriptLabel = takeNonBlankBegining(vars);
                var newLabel = varsToScriptLabel.join(';\r');
                if (newLabel !== mark.label) {
                    mark.label = newLabel;
                    ch++;
                }
                mark.insertLabel('VariableValues', vars.toSource());
                c++;
            }
        }
        var varNames = csvData.__meta.header.slice(1, csvData.__meta.header.length - 2);
        restoreUndefinedOn(varNames);
        L.insertLabel('VarsNames', varNames.toSource());
    }

    alert('Импортирована информация для ' + c + ' носителей.\n' +
        'У ' + ch + ' носителей ScriptLabel изменён.')
}

function restoreUndefinedOn(varNames) {
    for (var i = 0; i < varNames.length; ++i) {
        var V = varNames[i];
        if (V === 'undefined' || V === '\"undefined\"')
            varNames[i] = undefined;
    }
}

function takeNonBlankBegining(vars) {
    var lastNonBlank = 0;
    for (var i = 0; i < vars.length; ++i) {
        if (vars[i] !== '')
            lastNonBlank = i;
    }

    return vars.slice(0, lastNonBlank + 1);     // +1 to include last non blank 
}
