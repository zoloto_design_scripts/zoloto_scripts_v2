﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/checkMess.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/dataManipulations.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/JSON-js-master/json2.js');

var pathToJsonXls = scriptsFolder + '/zoloto_scripts_v2/not_indesign/json2xls.py';

var data = [];
app.doScript(main, undefined, undefined,
    UndoModes.entireScript, "ExportCSV");

function main() {
    var docIn = app.activeDocument;
    var layers = docIn.layers;
    var dataAsObj = {};
    dataAsObj.types = [];
    dataAsObj.data = {};

    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, layers.length);
    progress.maxvalue = layers.length;
    progress.value = 0;
    var files = 0;
    for (var l = 0; l < layers.length; ++l) {
        progress.value++;
        var L = layers[l];
        if (!L.visible)
            continue;

        data.length = 0;

        /*
        collectDataCsv(L.splineItems);
        sortMatrix(data, 3, compNumberingLabels);

        if (data.shape - 5 <= 0) {
            alert('Прерывание\nНе обнаружено наполнение');
            return;
        }
        
        var header = generateHeader(L, data.shape-5);
        */

        collectDataCsvNoStaff(L.splineItems);
        sortMatrix(data, 0, compNumberingLabels);

        if (data.shape - 2 <= 0) {
            alert('Ошибка\n'
                + 'Чтобы скрипт сработал, нужно оставить включенными '
                + 'только слои с объектами, для которых выгружается таблица.\n'
                + 'Также ошибка возникает, если включены слои без объектов.');
            return;
        }

        var header = generateHeader(L, data.shape - 2, ['id']);

        var csvPath = getCsvPath(docIn, L.name);
        var csvFile = new File(csvPath);
        writeCSV_header(csvFile, data, header, [1, 1]);
        ++files;
    }
    alert('Создано файлов: ' + files + '\n(в папке с исходным документом)');
}

