import os.path
import json
import xlwt
from datetime import datetime
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import askokcancel


def main():
    file_to_process_full_name = askopenfilename(initialdir=".",
                                                title="Выберите json файл с экспортированными данными",
                                                filetypes=(("json files", "*.json"), ("all files", "*.*")) )
    json_file = open(file_to_process_full_name)

    json_data = json.load(json_file)
    wb = xlwt.Workbook()
    write_info_into_table(json_data, wb)

    name = generate_table_filename(json_data)
    file = os.path.join(os.path.dirname(file_to_process_full_name), '..', name)
    if os.path.isfile(file):
        if askokcancel('Требуется подтверждение',
                       'Такой файл уже существует. Перезаписать?'):
            wb.save(file)
    else:
        wb.save(file)


def write_info_into_table(info, wb):
    types_exported = info['types']
    for carrier_type in types_exported:
        ws = wb.add_sheet(carrier_type)
        vars = info['data'][carrier_type]['data']

        max_key = 0
        r = 1

        # чтобы выводить упорядоченно по number берём список
        # ключей и сортируем глядя на соотв. number, а точнее его
        # последнюю часть с порядковым номером носителя
        def key_sort_from_number(number):
            parts = number.split('/')
            floor_name = '/'.join(parts[1:-1])
            return (floor_name, int(parts[-1]))
        keys_sorted = sorted(vars.keys(), key=lambda k: key_sort_from_number(vars[k]['number']))
        for key in keys_sorted:
            entry = vars[key]
            ws.write(r, 0, entry['id'])
            ws.write(r, 1, entry['number'])
            i = 0
            while str(i) in entry:
                if i > max_key:
                    max_key = i
                ws.write(r, i + 2, entry[str(i)])
                i += 1
            r += 1

        ws.write(0, 0, 'id')
        ws.write(0, 1, 'number')
        for i in range(max_key + 1):
            ws.write(0, i + 2, str(i + 1))


def generate_table_filename(info):
    dot_parts = info['file'].split('.')
    dot_parts[-1] = '.xls'
    origin_file_name = '.'.join(dot_parts)
    return origin_file_name


if __name__ == '__main__':
    main()
