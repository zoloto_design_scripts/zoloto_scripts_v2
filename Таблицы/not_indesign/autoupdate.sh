#! /bin/bash

#этот скрипт включает автообновление репозитрия каждые 5 минут
printf "%q\n" "$(pwd)"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )";
SDIR=`printf "%q\n" "$DIR"`
#write out current crontab
crontab -l > mycron
#echo new cron into cron file
echo "*/5 * * * * cd "$SDIR" && git pull" >> mycron
#install new cron file
crontab mycron
rm mycron

