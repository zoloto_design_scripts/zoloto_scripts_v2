﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/checkMess.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/dataManipulations.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/JSON-js-master/json2.js');

var pathToJsonXls = scriptsFolder + '/zoloto_scripts_v2/not_indesign/json2xls.py';

var data = [];
app.doScript(main, undefined, undefined,
    UndoModes.entireScript, "Export");

function main() {
    var docIn = app.activeDocument;
    var layers = docIn.layers;
    var dataAsObj = {};
    dataAsObj.types = [];
    dataAsObj.data = {};
    for (var l = 0; l < layers.length; ++l) {
        var L = layers[l];
        if (!L.visible)
            continue;

        data.length = 0;
        collectData(L.splineItems);

        dataAsObj.data[L.name] = dataObj(data);
        dataAsObj.types.push(L.name);
    }
    dataAsObj.file = docIn.name;

    var jsonPath = getJsonPath(docIn);
    var jsonFile = new File(jsonPath);
    var jsonData = JSON.stringify(dataAsObj);
    writeFile(jsonFile, jsonData, 'utf8');

    var jsonXlsScript = new File(pathToJsonXls);
    jsonXlsScript.execute();
}

function getJsonPath(docIn) {
    var origPath = docIn.fullName.fullName;
    origPath = origPath.split('.');
    origPath.pop();
    origPath.push('json');
    return origPath.join('.');
}

function dataObj(dataAsArray) {
    var buf = {};
    for (var i = 0; i < dataAsArray.length; ++i) {
        var entry = dataAsArray[i];
        var key = entry.id;
        buf[key] = {};
        for (var j = 0; j < entry.length; ++j)
            buf[key][j] = entry[j];
        buf[key].id = entry.id;
        buf[key].number = entry.number;
    }

    var st = {};
    var statistics = dataAsArray.statistics;
    for (var k in statistics)
        st[k] = statistics[k];

    var res = {};
    res.data = buf;
    res.statistics = st;
    return res;
}

