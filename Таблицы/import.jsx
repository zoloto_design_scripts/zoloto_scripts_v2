﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/JSON-js-master/json2.js');

var jsonPath = 'indesign_import.json';

app.doScript(main, undefined, undefined,
    UndoModes.entireScript, "Import");

function main() {
    var docIn = app.activeDocument;

    var jsonFile = new File(docIn.filePath + '/' + jsonPath);
    jsonFile.open('r');
    var jsonData = JSON.parse(jsonFile.read());

    var types = jsonData.types;
    var dataEachLayer = jsonData.data;
    var c = 0;
    var ch = 0;
    for (var t = 0; t < types.length; ++t) {
        var layerName = types[t];
        var layerCarriers = dataEachLayer[layerName].data;
        for (var id in layerCarriers) {
            var frame = docIn.splineItems.itemByID(Number(id));
            var carrier = layerCarriers[id];
            var carrierVars = arrayFromObj(carrier);
            var label = carrierVars.join(';\r');

            ++c;
            if (frame.label !== label) {
                ++ch;
                frame.label = label;
            }
        }
    }

    alert('Импортирована информация для ' + c + ' носителей.\n' +
        'У ' + ch + ' носителей ScriptLabel изменён.')
}

function arrayFromObj(obj) {
    var buf = {};
    for (var k in obj) {
        if (k === 'id' || k === 'number')
            continue;
        buf[k] = obj[k];
    }

    var arr = [];
    for (var n in buf)
        arr.push(buf[n]);
    return arr;
}