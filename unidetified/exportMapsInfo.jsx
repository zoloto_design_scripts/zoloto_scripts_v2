﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech /

    $.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/dataManipulations.jsx');

main();

function main() {
    var docIn = app.activeDocument;
    var mapsLayer = docIn.layers.itemByName('map');
    var captionslayer = docIn.layers.itemByName('floor');

    var captionIndex = {};
    var floorCaptions = captionslayer.textFrames;
    for (var c = 0; c < floorCaptions.length; ++c) {
        var caption = floorCaptions[c];
        var offset = caption.parentPage.documentOffset;     // documentOffset 0-based
        captionIndex[offset] = caption.contents;
    }

    var mapsBounds = {};
    var mapsGraphics = mapsLayer.allGraphics;
    for (var m = 0; m < mapsGraphics.length; ++m) {
        var map = mapsGraphics[m];
        var geometricBounds = map.geometricBounds;
        // var visibleBounds = map.visibleBounds;              // include stroke
        var offset = map.parentPage.documentOffset;
        mapsBounds[offset] = geometricBounds;
    }

    var data = [];
    for (var s = 0; s < docIn.pages.length; ++s) {
        if (!(s in captionIndex) && !(s in mapsBounds))
            continue;

        var caption = (s in captionIndex ? captionIndex[s] : '');
        var bounds = (s in mapsBounds ? mapsBounds[s] : ['', '', '', '']);
        data.push([s, caption].concat(bounds))
    }

    var csvPath = getCsvPath(docIn, 'maps_info');
    var csvFile = new File(csvPath);
    var header = ['document_offset', 'floor_caption', 'top', 'left', 'bottom', 'right'];
    writeCSV(csvFile, [header].concat(data));
    alert('Создан файл\nв папке с документом');
}

