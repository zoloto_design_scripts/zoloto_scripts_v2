﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/checkMess.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/dataManipulations.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/files.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/templateCases.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/generationHelpers.jsx');

var data = [];
var namePrefix;
var rootDir;
var placement = '/Расстановка';

var specialChars = {
    '&br': SpecialCharacters.FORCED_LINE_BREAK,
    '&tab': '\t'
};

main();

function main() {
    var docIn = app.activeDocument;
    namePrefix = docIn.name.split('_')[0] + '_' + docIn.name.split('_')[1] + '_';
    rootDir = docIn.filePath.parent;
    var layers = docIn.layers;

    var c = 0;
    for (var l = 0; l < layers.length; ++l) {
        var L = layers[l];
        if (!L.visible)
            continue;

        var csvPath = getCsvPath(docIn, L.name);
        var csvFile = new File(csvPath);
        data = parseCsvOnlyData(csvFile);

        var previewDoc = getPreviewDoc(docIn, data[0].length, L.name);
        if (!previewDoc) {
            alert('Проблемы при создании файла для вывода превью.\n' +
                'Вероятно не удалось создать папки мастер-файлов.');
            return;
        }

        replacePictsNames();
        var templatesUsages = collectTemplatesUsages();
        var templatesCasesDocs = getCasesDocActualized(templatesUsages);
        var p = generatePreviews(previewDoc, templatesCasesDocs);
        closeAllTemplateDocs(templatesCasesDocs);
        c++;
    }
    alert('Успешно созданы превью\n'
        + 'для ' + c + ' видимых слоёв');
}
