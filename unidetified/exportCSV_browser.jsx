﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech//tech//tech/

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/checkMess.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/dataManipulations.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/JSON-js-master/json2.js');

var pathToJsonXls = scriptsFolder + '/zoloto_scripts_v2/not_indesign/json2xls.py';

var data = [];
main();

function main() {
    var docIn = app.activeDocument;
    var layers = docIn.layers;
    var dataAsObj = {};
    dataAsObj.types = [];
    dataAsObj.data = {};

    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, layers.length);
    progress.maxvalue = layers.length;
    progress.value = 0;
    var files = 0;
    for (var l = 0; l < layers.length; ++l) {
        progress.value++;
        var L = layers[l];
        if (!L.visible)
            continue;

        data.length = 0;

        collectDataCsvBrowser(L.splineItems);
        sortMatrix(data, 0, compNumberingLabels);

        if (data.shape - 2 <= 0) {
            alert('Прерывание\nНе обнаружено наполнение');
            return;
        }

        var header = generateHeader(L, data.shape - 2, ['id']);

        var csvPath = getCsvPath(docIn, L.name);
        var csvFile = new File(csvPath);
        writeCSV_header(csvFile, data, header, [1, 1]);
        ++files;
    }
    alert('Создано файлов: ' + files + '\n(в папке с исходным документом)');
}

