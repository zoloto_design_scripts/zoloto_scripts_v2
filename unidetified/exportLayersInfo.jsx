﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech /

    $.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/JSON-js-master/json2.js');

main();

function main() {
    var docIn = app.activeDocument;
    var data = [];
    var layers = docIn.layers;
    for (var l = 0; l < layers.length; ++l) {
        var L = layers[l];
        var color = deduceLayerColor(L);
        if (!color)
            continue;
        data.push([L.name, '', color]);
    }

    var csvPath = getCsvPath(docIn, 'layers_info');
    var csvFile = new File(csvPath);
    var header = ['layer_name', 'description', 'color'];
    writeCSV(csvFile, [header].concat(data));
    alert('Создан файл\nв папке с документом');
}


function deduceLayerColor(layer) {
    var splineItems = layer.splineItems;
    if (splineItems.length > 0) {
        var color = splineItems[0].fillColor;
        if (!(color instanceof Color))
            return undefined;
        return '(' + color.space.toString() + ', ' + color.colorValue.toString() + ')';
    }
    else
        return undefined;
}
