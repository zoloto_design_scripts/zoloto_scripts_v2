﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/ / tech /

    $.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/files.jsx');

main();

function main() {
    var doc = app.activeDocument;
    var pdfFolder = doc.filePath + '/export/';
    createDir_alert(pdfFolder);
    var pages = doc.pages;

    var mapOffsetToFilename = scanNumberMarks(doc);
    if (!mapOffsetToFilename)
        return;
    configureExport();

    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, pages.length);
    for (var p = 0; p < pages.length; ++p) {
        var file = new File(pdfFolder + '/' + mapOffsetToFilename[p] + '.pdf');
        app.pdfExportPreferences.pageRange = String(pages[p].name);
        doc.exportFile(ExportFormat.PDF_TYPE, file);
        progress.value++;
    }
    alert('Экспорт выполнен\n' + 'Создано ' + pages.length + ' файлов');
}

function scanNumberMarks(doc) {
    var numbersLayer = doc.layers.itemByName('numbers');

    var offsetToFilename = {};
    var trackFilenames = {};
    var repeated = [];
    var marks = numbersLayer.textFrames;
    for (var m = 0; m < marks.length; ++m) {
        var mark = marks[m];
        var filename = replaceSlashDots_Spaces(mark.contents);
        offsetToFilename[mark.parentPage.documentOffset] = filename;

        if (trackFilenames[filename] !== undefined)
            repeated.push(filename);
        else
            trackFilenames[filename] = [];
        trackFilenames[filename].push(mark.parentPage.documentOffset);
    }

    if (repeated.length > 0) {
        alert('Ошибка: есть повторы подписей носителей\n' +
            repeated.join(', '));
        return undefined;
    }
    else
        return offsetToFilename;
}

function configureExport() {
    app.pdfExportPreferences.exportWhichLayers = ExportLayerOptions.EXPORT_VISIBLE_LAYERS;
    app.pdfExportPreferences.acrobatCompatibility = AcrobatCompatibility.ACROBAT_6;
    app.pdfExportPreferences.exportLayers = true;
    app.pdfExportPreferences.viewPDF = false;

    /*fltOutlinesName = 'High Resolution with outline text';
    var fltOutlines = app.flattenerPresets.itemByName(fltOutlinesName);
    if (!fltOutlines.isValid) {
        fltOutlines = app.flattenerPresets.add();
        fltOutlines.name = fltOutlinesName;
        fltOutlines.rasterVectorBalance = 75;
        fltOutlines.lineArtAndTextResolution = 2400;
        fltOutlines.gradientAndMeshResolution = 144;
        fltOutlines.convertAllTextToOutlines = true;
        fltOutlines.convertAllStrokesToOutlines = true;
        fltOutlines.clipComplexRegions = false;
    }
    app.pdfExportPreferences.appliedFlattenerPreset = fltOutlines;*/
}

function replaceSlashDots_Spaces(filename) {
    while (filename.indexOf('/') !== -1)
        filename = filename.replace('/', '_');
    while (filename.indexOf('.') !== -1)
        filename = filename.replace('.', '_');
    while (filename.indexOf(' ') !== -1)
        filename = filename.replace(' ', '_');
    return filename;
}
