﻿var scriptsFolder = app.scriptPreferences.scriptsFolder; /tech/

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/checkMess.jsx');

main();

function main() {
    var docIn = this.app.activeDocument;
    var activeLayer = docIn.activeLayer;
    var sItems = activeLayer.splineItems;
    var modified = 0;
    for (var s = 0; s < sItems.length; ++s) {
        var item = sItems[s];
        var content = item.label;

        var res = checkMess(content);
        if (res.changed) {
            item.label = res.text;
            ++modified;
        }
    }
    alert('Done' +
        '\nПодправлено ' + modified + ' носителей из ' + sItems.length + '.');
}
