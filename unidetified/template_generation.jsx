﻿// имеем объект описания случаев использования шаблонов/tech//tech//tech//tech//tech/
// по имени шаблона находим его мастер файл и
// в папке Шаблоны создаём файл с этим именем и timestamp-ом 
// и там создаём по странице с заданной masterpage для каждого случая

var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/checkMess.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/dataManipulations.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/files.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/include/templateCases.jsx');

var data = [];

var rootDir;

main();

function main() {
    var docIn = app.activeDocument;
    rootDir = docIn.filePath.parent;

    var namePrefix = docIn.name.split('_')[0] + '_' + docIn.name.split('_')[1] + '_';
    var pictPath = docIn.filePath + '/' + namePrefix + 'pict_codes.csv';
    var pictFile = getCSV(pictPath);
    if (!pictFile)
        return;

    var namesPath = docIn.filePath + '/' + namePrefix + 'poi_names.csv';
    var namesFile = getCSV(namesPath);
    if (!namesFile)
        return;

    var pictDict = parseCSV(pictFile);
    var namesDict = parseCSV(namesFile);

    var selection = docIn.activeLayer.splineItems;
    collectData(selection);
    replaceByMap(data, pictDict);
    replaceByMultimap(data, namesDict);

    var templates = templatesCollectReplace();
    var allCases = 0;
    for (var tName in templates)
        allCases += templates[tName].length;

    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, allCases);
    for (var tName in templates) {
        var cases = templates[tName];
        tName = tName.slice(1);

        generateOneTemplateCases(tName, cases);
        progress.value += cases.length;
    }
}

function templatesCollectReplace() {
    var templateUsages = {};
    for (var p = 0; p < data.length; ++p) {
        for (var v = 0; v < data[p].length; ++v) {
            var field = data[p][v];

            var pr = field.indexOf('%');
            while (pr !== -1) {
                var bo = field.indexOf('(', pr);
                var bc = field.indexOf(')', pr);
                if (bo === -1 || bc === -1)
                    break;

                var name = field.slice(pr, bo);
                var args = field.slice(bo, bc + 1);
                if (!templateUsages[name]) {
                    templateUsages[name] = {};
                    templateUsages[name].length = 0;
                }
                if (!templateUsages[name][args]) {
                    var n = templateUsages[name].length;
                    templateUsages[name][args] = n;
                    //templateUsages[name][n] = args;
                    ++templateUsages[name].length;
                }

                // теперь нужно заменить список аргументов на номер использования
                field = field.slice(0, bo + 1) + templateUsages[name][args] + field.slice(bc);
                pr = field.indexOf('%', bc);
            }
            data[p][v] = field;
        }
    }
    return templateUsages;
}

