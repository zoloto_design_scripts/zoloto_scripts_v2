var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/checkMess.jsx');

var data = [];

main();

function main() {
    var docIn = app.activeDocument;
    var selection = docIn.activeLayer.splineItems;
    collectData(docIn.selection);
    var p = countPicts();
    alert('Выделено ' + selection.length + ' носителей.' +
        '\nНа них ' + p + ' пиктограмм.');
}

function collectData(selection) {
    var shape = 0;
    var nVars = {};
    for (var i = 0; i < selection.length; ++i) {
        var mark = selection[i];
        var label = checkMess(mark.label).text;
        var vars = label.split(';\r');
        vars.number = mark.extractLabel('number');
        data.push(vars);

        var len = vars.length;
        if (len > shape)
            shape = len;
        if (nVars[len] === undefined)
            nVars[len] = 0;
        ++nVars[len];
    }
    data.shape = shape;
    data.statistics = nVars;
}

function countPicts() {
    var p = 0;
    for (var n = 0; n < data.length; ++n) {
        for (var v = 0; v < data[n].length; ++v) {
            var line = data[n][v];
            for (var i = 0; i < line.length; ++i)
                if (line.charAt(i) === '@')
                    ++p;
        }
    }
    return p;
}