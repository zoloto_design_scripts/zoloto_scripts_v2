var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');

main();

function main() {
    var docIn = this.app.activeDocument;
    var markers = docIn.activeLayer.splineItems;

    var documentName = docIn.name;
    var sourceLayerName = docIn.activeLayer.name;
    var header = documentName + '_' + sourceLayerName + '_' + 'length' + getDatePostfix();
    var outputLine = '\uFEFF' + header + '\n';

    var linearFactor = 610;
    for (var s = 0; s < markers.length; s++) {
        var splitem = markers.item(s);
        var path = splitem.paths.item(0).entirePath;
        var L = 0;
        for (var i = 0; i < path.length; i++) {
            if (i !== 0)
                var prevPoint = point;

            var multipoint = path[i];
            if (typeof multipoint[0] === 'number')
                var point = multipoint;
            else
                var point = multipoint[1];

            if (i !== 0) {
                L += Math.sqrt(
                    Math.pow((point[0] - prevPoint[0]), 2) +
                    Math.pow((point[1] - prevPoint[1]), 2));
            }
        }
        L *= linearFactor;
        L = Math.floor(L);

        var name = splitem.label;
        outputLine += name + ' ' + L + '\n';
    }

    var pathToActive = docIn.filePath.path;
    var file = new File(pathToActive + '/' + header + '.txt');
    writeFile(file, outputLine, 'UTF-16LE');
    alert('Измерения сохранены в файл \'' + header);
    return 'done';
}
