var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/checkMess.jsx');

var data = [];

main();

function main() {
    var docIn = app.activeDocument;
    var selection = docIn.activeLayer.splineItems;
    collectData(docIn.selection);
    var n = countVars();
    alert('Выделено ' + selection.length + ' носителей.' +
        '\nНа них ' + n + ' переменных.');
}

function collectData(selection) {
    var shape = 0;
    var nVars = {};
    for (var i = 0; i < selection.length; ++i) {
        var mark = selection[i];
        var label = checkMess(mark.label).text;
        var vars = label.split(';\r');
        vars.number = mark.extractLabel('number');
        data.push(vars);

        var len = vars.length;
        if (len > shape)
            shape = len;
        if (nVars[len] === undefined)
            nVars[len] = 0;
        ++nVars[len];
    }
    data.shape = shape;
    data.statistics = nVars;
}

function countVars() {
    var n = 0;
    for (var len in data.statistics)		// кол-во носителей с таким
        n += data.statistics[len] * len; 	// числом переменных * число переменных
    return n;
}