﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/enumerate.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/geometry.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/captions.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/styles.jsx');

// ширина колонок таблицы сообщения
var defaultTableParams = { left: 3, right: 16 };
var tableParams = {};

// вектор от правой точки маркера к левой нижней таблицы
var tableRel = [3, 3];
var arrVarsNames = [];
var useVarsNames = false;

app.doScript(main, undefined, undefined,
    UndoModes.entireScript, "ShowMessTable");

function main() {
    var doc = app.activeDocument;
    var layer = doc.activeLayer;
    var origName = layer.name;
    var markers = layer.splineItems;

    if (!enumerate(doc))
        return;

    var styles = getTableStyles(doc.paragraphStyles, doc.cellStyles, doc.tableStyles, origName);
    if (!styles)
        return;

    // сбор из labels слоя и актуализация параметров вывода таблиц
    if (!configureTableView(layer)) {
        alert('Отменено.');
        return;
    }

    var tablesLayer = createCaptionLayer(doc.layers, origName, layer.layerColor, 'tables');

    generateCaptions(markers, tablesLayer, styles, 'Table');

    doc.activeLayer = layer;
    // doc.save();
    alert('Выполнено.\nПронумеровано ' + markers.length + ' объектов.');
}

function createCaption(marker, styles, captionsLayer) {
    // tableRel = [10, 10] вектор от правой точки маркера к верхней левой точке таблицы [x, y]
    // ширина колонок таблицы tableParams = {left: Number, right: Number};
    var height = 100;
    var mostRight = takeMostRight(marker); // [x, y]
    var left = mostRight[0] + tableRel[0];
    var top = mostRight[1] + tableRel[1];
    var bottom = top + height;
    var right = left + (Number(tableParams.left) + Number(tableParams.right) + 5);

    var tableFrame = marker.parentPage.textFrames.add();
    tableFrame.move(captionsLayer);
    tableFrame.insertLabel('MarkerId', marker.id.toString());
    tableFrame.geometricBounds = [top, left, bottom, right];

    var table = tableFrame.tables.add({
        bodyRowCount: 1,
        columnCount: 2,
    });
    table.columns[1].autoGrow = true;
    table.appliedTableStyle = styles.table;
    return tableFrame;
}

function modifyCaption(tableFrame, marker) {
    var table = tableFrame.tables[0];
    var cols = table.columns;
    cols[0].width = tableParams.left;
    cols[1].width = tableParams.right;

    var vars = marker.label.split(';\r');
    table.bodyRowCount = vars.length + 1;   // в первой строке подпись номера, потом vars.length переменных
    var rows = table.rows;
    rows[0].contents = ['number', marker.extractLabel('Number')];
    for (var v = 0; v < vars.length; ++v) {
        var rowLabel = (useVarsNames && v < arrVarsNames.length
            ? arrVarsNames[v] : String(v + 1));
        rows[v + 1].contents = [rowLabel, vars[v]];
    }
    tableFrame.fit(FitOptions.FRAME_TO_CONTENT);
}

function askParams(oldParams) {
    var params = {};

    var w = new Window('dialog', 'Настройка таблицы');
    w.pnlClmns = w.add('panel', undefined, 'Колонки, ширина (мм)');
    w.pnlClmns.alignChildren = 'right';
    w.pnlClmns.grLeft = w.pnlClmns.add('group');
    w.pnlClmns.grLeft.lbl = w.pnlClmns.grLeft.add('statictext', undefined, 'Левая: ');
    w.pnlClmns.grLeft.val = w.pnlClmns.grLeft.add('edittext');
    w.pnlClmns.grLeft.val.characters = 5;
    w.pnlClmns.grLeft.val.text = oldParams.left;

    w.pnlClmns.grRight = w.pnlClmns.add('group');
    w.pnlClmns.grRight.lbl = w.pnlClmns.grRight.add('statictext', undefined, 'Правая: ');
    w.pnlClmns.grRight.val = w.pnlClmns.grRight.add('edittext');
    w.pnlClmns.grRight.val.characters = 5;
    w.pnlClmns.grRight.val.text = oldParams.right;

    w.grBtns = w.add('group');
    w.grBtns.alignment = 'right';
    w.grBtns.btnCancel = w.grBtns.add('button', undefined, 'Отмена');
    w.grBtns.btnCancel.onClick = function () {
        params = undefined;
        w.close();
    }

    w.grBtns.btnOK = w.grBtns.add('button', undefined, 'ОК');
    w.grBtns.btnOK.onClick = function () {
        params.left = w.pnlClmns.grLeft.val.text;
        params.right = w.pnlClmns.grRight.val.text;
        w.close();
    }
    w.grBtns.btnOK.active = true;
    w.show();
    return params;
}

function configureTableView(layer) {
    // если слой не имеет ключа TableParams, берём выставляем его по значению по умолчанию
    var lblTableParams = layer.extractLabel('TableParams');
    if (lblTableParams && lblTableParams !== '')
        this.defaultTableParams = eval(lblTableParams);
    tableParams = askParams(this.defaultTableParams);
    if (!tableParams)
        return false;
    layer.insertLabel('TableParams', tableParams.toSource());

    // если слой имеет ключ VarsNames, ставим флаг useVarsNames и запоминаем значение в arrVarsNames
    var lblVarsNames = layer.extractLabel('VarsNames');
    if (lblVarsNames && lblVarsNames !== '') {
        arrVarsNames = eval(lblVarsNames);
        if (arrVarsNames.length !== undefined)
            useVarsNames = true;
        else
            arrVarsNames = [];
    }
    return true;
}
