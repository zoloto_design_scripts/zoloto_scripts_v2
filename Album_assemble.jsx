﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/common.jsx');
$.evalFile(scriptsFolder + '/zoloto_scripts_v2/tech/files.jsx');

app.doScript(main, ScriptLanguage.JAVASCRIPT, undefined,
    UndoModes.entireScript, "Album assemble");

function main() {
    var z_filteredFiles;
    z_extensions = [".png", ".bmp", ".ch", ".sct", ".jpg", ".jpeg", ".eps", ".ps", ".pdf", ".tif", ".tiff", ".gif", ".psd", ".ai", ".PNG", ".BMP", ".CH", ".SCT", ".PS", ".TIF", ".TIF", ".GIF", ".PSD", ".AI"]
    
    var z_folder = Folder.selectDialog("Выберите папку, содержащую изображения", "");

    if(z_folder != null){
        if (File.fs == "Macintosh"){
            z_filteredFiles = z_MacOSFileFilter(z_folder);
        } else {
            z_filteredFiles = z_WinOSFileFilter(z_folder);
        }

        z_filteredFiles = z_filteredFiles.sort(compareFiles)

        if (z_filteredFiles.length != 0) {
            z_displayDialog(z_filteredFiles, z_folder);
        }
    }
}

function z_WinOSFileFilter(z_folder) {
    var z_files = new Array;
    var z_filteredFiles = new Array;
    for (z_extensionCounter = 0; z_extensionCounter < z_extensions.length; z_extensionCounter++) {
        z_extension = z_extensions[z_extensionCounter];
        z_files = z_folder.getFiles("*" + z_extension);
        if (z_files.length != 0) {
            for (var fileCounter = 0; fileCounter < z_files.length; fileCounter++){
                z_filteredFiles.push(z_files[fileCounter]);
            }
        }
    }

    return z_filteredFiles;
}

function z_MacOSFileFilter(z_folder) {
    var z_filteredFiles = z_folder.getFiles(fileFilter);
    return z_filteredFiles;
}

function fileFilter(z_file){
    var z_fileType = z_file.type;
    switch (z_fileType){
        case "PNG":
        case "BMP":
        case "CH":
        case "SCT":
        case "JPEG":
        case "EPSF":
        case "PICT":
        case "TIFF":
        case "8BPS":
        case "GIFf":
        case "PDF ":
            return true;
            break;
        default:
            for (var z_Counter = 0; z_Counter < z_extensions.length; z_Counter++) {
                var z_extension = z_extensions[z_Counter];
                if (z_file.name.indexOf(z_extension) > -1) {
                    return true;
                    break;
                }
            }
    }
    return false;
}

function compareFiles(a, b) {
    var a_arr = a.name.split('.')[0].split('');
    var b_arr = b.name.split('.')[0].split('');

    for (var i = 0; i < a_arr.length; i++) {
        // случай когда "..._12", "..._1" и "..._12", "..._5"
        if (!b_arr[i] || (a_arr[i+1] && !b_arr[i+1])) return 1;
        // случай "..._5", "..._51" или "..._5", "..._12"
        if (!a_arr[i+1] && b_arr[i+1]) return -1;
        // если на данной букве-цифре одинаковы, и предыдущие условия не выполняются - смотрим следующую букву-цифру
        if (a_arr[i] == b_arr[i]) continue;
        // тут уже обычное сравнение типа 1 < 5
        if (a_arr[i] > b_arr[i]) return 1;
        else return -1;
    }

    return 0;
}

function z_displayDialog(z_files, z_folder) {
    var labelWidth = 112;
    var styleNames = getParagraphStyleNames(app);

    var masterSpreadNames = getMSNames();
    var msIndex = getMSIndex(masterSpreadNames);

    var layerNames = getLayerNames();

    var z_dialog = app.dialogs.add({ name: "Галерея" });

    with (z_dialog.dialogColumns.add()) {
        with (dialogRows.add()) {
            staticTexts.add({ staticLabel: "Выбор мастер-страницы:" });
        }

        with (borderPanels.add()) {
            with (dialogRows.add()) {
                with (dialogColumns.add()) {
                    staticTexts.add({ staticLabel: "Мастер-страница:", minWidth: labelWidth });
                }
                with (dialogColumns.add()) {
                    var masterSpreadDropdown = dropdowns.add({ stringList: masterSpreadNames, selectedIndex: msIndex });
                }
            }
        }

        with (dialogRows.add()) {
            staticTexts.add({ staticLabel: "Информация:" });
        }

        with (borderPanels.add()) {
            with (dialogColumns.add()) {
                with (dialogRows.add()) {
                    staticTexts.add({ staticLabel: "Исходная папка:", minWidth: labelWidth });

                    var arr = z_folder.path.split("/");
                    var l = arr.length;
                    staticTexts.add({ staticLabel: arr.slice(l - 2, l).join("/") + "/" + z_folder.name });
                }
                with (dialogRows.add()) {
                    staticTexts.add({ staticLabel: "Количество изображений:", minWidth: labelWidth });
                    staticTexts.add({ staticLabel: z_files.length + "" });
                }
            }
        }

        with (dialogRows.add()) {
            staticTexts.add({ staticLabel: "Опции:" });
        }

        with (borderPanels.add()) {
            with (dialogColumns.add()) {
                with (dialogRows.add()) {
                    staticTexts.add({ staticLabel: "Число строк:", minWidth: labelWidth });
                    var numberOfRowsField = integerEditboxes.add({ editValue: 1 });
                }
                with (dialogRows.add()) {
                    staticTexts.add({ staticLabel: "Число колонок:", minWidth: labelWidth });
                    var z_numberOfColumnsField = integerEditboxes.add({ editValue: 4 });
                }
                with (dialogRows.add()) {
                    staticTexts.add({ staticLabel: "Горизонтальное смещение:", minWidth: labelWidth });
                    var horizontalOffsetField = measurementEditboxes.add({ editValue: 0, editUnits: MeasurementUnits.points });
                }
                with (dialogRows.add()) {
                    staticTexts.add({ staticLabel: "Вертикальное смещение:", minWidth: labelWidth });
                    var verticalOffsetField = measurementEditboxes.add({ editValue: 24, editUnits: MeasurementUnits.points });
                }
                with (dialogRows.add()) {
                    var floorsControl = checkboxControls.add({ staticLabel: "Учитывать этажи", checkedState: false });
                }

                with (dialogRows.add()) {
                    with (dialogColumns.add()) {
                        staticTexts.add({ staticLabel: "Положение:", minWidth: labelWidth });
                    }
                    with (dialogColumns.add()) {
                        var fitProportionalCheckbox = checkboxControls.add({ staticLabel: "Пропорционально", checkedState: true });
                        var fitCenterContentCheckbox = checkboxControls.add({ staticLabel: "Центрировать", checkedState: true });
                        var fitFrameToContentCheckbox = checkboxControls.add({ staticLabel: "Frame to Content", checkedState: true });
                    }
                }
                with (dialogRows.add()) {
                    var z_removeEmptyFramesCheckbox = checkboxControls.add({ staticLabel: "Удалить пустые кадры:", checkedState: true });
                }
            }
        }
        with (dialogRows.add()) {
            staticTexts.add({ staticLabel: "" });
        }
        var labelsGroup = enablingGroups.add({ staticLabel: "Заголовок", checkedState: true });
        with (labelsGroup) {
            with (dialogColumns.add()) {

                with (dialogRows.add()) {
                    with (dialogColumns.add()) {
                        staticTexts.add({ staticLabel: "Тип заголовка:", minWidth: labelWidth });
                    }
                    with (dialogColumns.add()) {
                        var labelTypeDropdown = dropdowns.add({ stringList: ["Имя файла", "Путь к файлу", "XMP описание", "XMP автор"], selectedIndex: 0 });
                    }
                }

                with (dialogRows.add()) {
                    with (dialogColumns.add()) {
                        staticTexts.add({ staticLabel: "Высота заголовка:", minWidth: labelWidth });
                    }
                    with (dialogColumns.add()) {
                        var labelHeightField = measurementEditboxes.add({ editValue: 10, editUnits: MeasurementUnits.points });
                    }
                }

                with (dialogRows.add()) {
                    with (dialogColumns.add()) {
                        staticTexts.add({ staticLabel: "Отступ заголовка:", minWidth: labelWidth });
                    }
                    with (dialogColumns.add()) {
                        var labelOffsetField = measurementEditboxes.add({ editValue: 3, editUnits: MeasurementUnits.points });
                    }
                }

                with (dialogRows.add()) {
                    with (dialogColumns.add()) {
                        staticTexts.add({ staticLabel: "Стиль заголовка:", minWidth: labelWidth });
                    }
                    with (dialogColumns.add()) {
                        var labelStyleDropdown = dropdowns.add({ stringList: styleNames, selectedIndex: styleNames.length - 1 });
                    }
                }

                with (dialogRows.add()) {
                    with (dialogColumns.add()) {
                        staticTexts.add({ staticLabel: "Слой:", minWidth: labelWidth });
                    }
                    with (dialogColumns.add()) {
                        var layerDropdown = dropdowns.add({ stringList: layerNames, selectedIndex: 0 });
                    }
                }
            }
        }

        var z_result = z_dialog.show();
        if(z_result == true) {
            var z_numberOfRows = numberOfRowsField.editValue;
            var z_numberOfColumns = z_numberOfColumnsField.editValue;
            var z_removeEmptyFrames = z_removeEmptyFramesCheckbox.checkedState;
            var z_fitProportional = fitProportionalCheckbox.checkedState;
            var z_FitCenterContent = fitCenterContentCheckbox.checkedState;
            var z_FitFrameToContent = fitFrameToContentCheckbox.checkedState;
            var z_HorizontalOffset = horizontalOffsetField.editValue;
            var z_VerticalOffset = verticalOffsetField.editValue;
            var z_MakeLabels = labelsGroup.checkedState;
            var z_LabelType = labelTypeDropdown.selectedIndex;
            var z_LabelHeight = labelHeightField.editValue;
            var z_LabelOffset = labelOffsetField.editValue;
            var z_LabelStyle = styleNames[labelStyleDropdown.selectedIndex];
            var z_LayerName = layerNames[layerDropdown.selectedIndex];
            var z_masterSpreadName = masterSpreadNames[masterSpreadDropdown.selectedIndex];
            var z_considerFloors = floorsControl.checkedState;
            if (z_considerFloors) {
                z_dialog.destroy();
                var newFiles = splitByFloors(z_files);

                for (var i = 0; i < newFiles.length; i++) {
                    z_MakeImageCatalog(newFiles[i], z_numberOfRows, z_numberOfColumns, z_removeEmptyFrames, z_fitProportional, z_FitCenterContent, z_FitFrameToContent, z_HorizontalOffset, z_VerticalOffset, z_MakeLabels, z_LabelType, z_LabelHeight, z_LabelOffset, z_LabelStyle, z_LayerName, z_masterSpreadName, z_considerFloors);
                }
            } else {

                z_dialog.destroy();
                z_MakeImageCatalog(z_files, z_numberOfRows, z_numberOfColumns, z_removeEmptyFrames, z_fitProportional, z_FitCenterContent, z_FitFrameToContent, z_HorizontalOffset, z_VerticalOffset, z_MakeLabels, z_LabelType, z_LabelHeight, z_LabelOffset, z_LabelStyle, z_LayerName, z_masterSpreadName, z_considerFloors);

            }
        }
        else{
            z_dialog.destroy();
        }
    }
}

function splitByFloors(files) {
    var result = [];
    var filesByNames = {};
    var names = [];

    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var nameArr = file.fsName.toString().split('_');
        var l = nameArr.length;
        var name = nameArr.slice(0, l-2).join('_');
        if (filesByNames[name]) {
            filesByNames[name].push(file);
        } else {
            filesByNames[name] = [];
            filesByNames[name].push(file);
            names.push(name);
        }
    }

    for (var j = 0; j < names.length; j++) {
        var n = names[j];
        result.push(filesByNames[n]);
    }

    return result;
}

function getMSNames() {
    var ms = app.activeDocument.masterSpreads;
    var result = new Array;

    for (var i = 0; i < ms.length; i++) {
        result.push(ms[i].name);
    }
    return result;
}

function getMSIndex(names) {
    for (var i = 0; i < names.length; i++) {
        if (names[i].indexOf("3.2") == 0) return i;
    }

    for (var i = 0; i < names.length; i++) {
        if (names[i].indexOf("3") == 0) return i;
    }
    return 0;
}

function getParagraphStyleNames(z_Document){
    var styleNames = new Array;
    var z_AddLabelStyle = true;
    for(var z_Counter = 0; z_Counter < z_Document.paragraphStyles.length; z_Counter++){
        styleNames.push(z_Document.paragraphStyles.item(z_Counter).name);
        if (z_Document.paragraphStyles.item(z_Counter).name == "Labels"){
            z_AddLabelStyle = false;
        }
    }
    if(z_AddLabelStyle == true){
        styleNames.push("Labels");
    }
    return styleNames;
}

function getLayerNames() {
    var layers = app.activeDocument.layers;
    var mainLayer = app.activeDocument.activeLayer;
    var mainLayerName = mainLayer.name;

    var layerNames = [];
    layerNames.push(mainLayerName);

    for (var i = 0; i < layers.length; i++) {
        if (layers[i].name === mainLayerName) continue;
        layerNames.push(layers[i].name);
    }

    return layerNames;
}

function z_MakeImageCatalog(z_files, z_numberOfRows, z_numberOfColumns, z_removeEmptyFrames, z_fitProportional, z_FitCenterContent, z_FitFrameToContent, z_HorizontalOffset, z_VerticalOffset, z_MakeLabels, z_LabelType, z_LabelHeight, z_LabelOffset, z_LabelStyle,  z_LayerName, z_masterSpread, z_floors){
    if (!z_masterSpread) {
        alert("Мастер-страница не выбрана.");
        return;
    }

    var z_Page, z_file, z_Counter, z_X1, z_Y1, z_X2, z_Y2, z_Rectangle, z_LabelStyle, z_LabelLayer;
    var z_ParagraphStyle, z_Error;
    var z_FramesPerPage = z_numberOfRows * z_numberOfColumns;
    var z_Document = app.activeDocument;

    var z_DocumentPreferences = z_Document.documentPreferences;
    var z_NumberOfFrames = z_files.length;
    var z_NumberOfPages = Math.round(z_NumberOfFrames / z_FramesPerPage);
    if ((z_NumberOfPages * z_FramesPerPage) < z_NumberOfFrames){
        z_NumberOfPages++;
    }
    var initialNumberOfPages = z_DocumentPreferences.pagesPerDocument;

    if(z_MakeLabels == true){
        try{
            z_LabelLayer = z_Document.layers.item(z_LayerName);

            z_LabelLayer.name;
        }
        catch (z_Error){
            z_LabelLayer = z_Document.layers.add({name:z_LayerName});
        }

        try{
            z_ParagraphStyle = z_Document.paragraphStyles.item(z_LabelStyle);
            z_ParagraphStyle.name;
        }
        catch(z_Error){
            z_Document.paragraphStyles.add({name:z_LabelStyle});
        }
    }

    z_DocumentPreferences.facingPages = false;
    var ms = app.activeDocument.masterSpreads.itemByName(z_masterSpread);

    var patternPage = z_Document.pages.add();
    patternPage.appliedMaster = ms;
    var z_MarginPreferences = patternPage.marginPreferences;
    var z_LeftMargin = z_MarginPreferences.left;
    var z_TopMargin = z_MarginPreferences.top;
    var z_RightMargin = z_MarginPreferences.right;
    var z_BottomMargin = z_MarginPreferences.bottom;
    var z_LiveWidth = (z_DocumentPreferences.pageWidth - (z_LeftMargin + z_RightMargin)) + z_HorizontalOffset;
    var z_LiveHeight = z_DocumentPreferences.pageHeight - (z_TopMargin + z_BottomMargin);
    var z_ColumnWidth = z_LiveWidth / z_numberOfColumns;
    var z_FrameWidth = z_ColumnWidth - z_HorizontalOffset;
    var z_RowHeight = (z_LiveHeight / z_numberOfRows);
    var z_FrameHeight = z_RowHeight - z_VerticalOffset;
    patternPage.remove();

    var z_Pages = z_Document.pages;
    var rectangles = [];

    for (z_Counter = 0; z_Counter < z_NumberOfPages; z_Counter++) {
        z_Page = z_Pages.add();
        z_Page.appliedMaster = ms;
        for (var z_RowCounter = 1; z_RowCounter <= z_numberOfRows; z_RowCounter++) {
            z_Y1 = z_TopMargin + (z_RowHeight * (z_RowCounter-1));
            z_Y2 = z_Y1 + z_FrameHeight;
            for (var z_ColumnCounter = 1; z_ColumnCounter <= z_numberOfColumns; z_ColumnCounter++) {
                if (z_removeEmptyFrames && rectangles.length === z_NumberOfFrames) continue;

                z_X1 = z_LeftMargin + (z_ColumnWidth * (z_ColumnCounter-1));
                z_X2 = z_X1 + z_FrameWidth;
                z_Rectangle = z_Page.rectangles.add(z_Document.activeLayer, undefined, undefined, { geometricBounds:[z_Y1, z_X1, z_Y2, z_X2], strokeWeight:0, strokeColor:z_Document.swatches.item("None") });
                rectangles.push(z_Rectangle);
                z_Rectangle.fillTransparencySettings.blendingSettings.opacity = 0;
                z_Rectangle.strokeTransparencySettings.blendingSettings.opacity = 0;
            }
        }
    }

    for (z_Counter = 0; z_Counter < z_NumberOfFrames; z_Counter++) {
        z_file = z_files[z_Counter];
        z_Rectangle = rectangles[z_Counter];
        z_Rectangle.place(File(z_file));
        z_Rectangle.label = z_file.fsName.toString();

        if (z_fitProportional) {
            z_Rectangle.fit(FitOptions.proportionally);
        }
        if (z_FitCenterContent) {
            z_Rectangle.fit(FitOptions.centerContent);
        }
        if (z_FitFrameToContent) {
            z_Rectangle.fit(FitOptions.frameToContent);
        }

        if (z_MakeLabels == true) {
            z_AddLabel(z_Rectangle, z_LabelType, z_LabelHeight, z_LabelOffset, z_LabelStyle, z_LayerName);
        }
    }
}

function z_AddLabel(z_Frame, z_LabelType, z_LabelHeight, z_LabelOffset, z_LabelStyleName, z_LayerName){
    var z_Document = app.documents.item(0);
    var z_Label;
    var z_LabelStyle = z_Document.paragraphStyles.item(z_LabelStyleName);
    var z_LabelLayer = z_Document.layers.item(z_LayerName);
    var z_Link =z_Frame.graphics.item(0).itemLink;

    switch(z_LabelType){

        case 0:
            z_Label = z_Link.name;
            break;

        case 1:
            z_Label = z_Link.filePath;
            break;

        case 2:
            try{
                z_Label = z_Link.linkXmp.description;
            }
            catch(z_Error){
                z_Label = "No description available.";
            }
            break;

        case 3:
            try{
                z_Label = z_Link.linkXmp.author
            }
            catch(z_Error){
                z_Label = "No author available.";
            }
            break;
    }
    var z_X1 = z_Frame.geometricBounds[1];
    var z_Y1 = z_Frame.geometricBounds[2] + z_LabelOffset;
    var z_X2 = z_Frame.geometricBounds[3];
    var z_Y2 = z_Y1 + z_LabelHeight;
    var z_TextFrame = z_Frame.parent.textFrames.add(z_LabelLayer, undefined, undefined,{geometricBounds:[z_Y1, z_X1, z_Y2, z_X2], contents:z_Label});
    z_TextFrame.textFramePreferences.firstBaselineOffset = FirstBaseline.leadingOffset;
    z_TextFrame.paragraphs.item(0).appliedParagraphStyle = z_LabelStyle;
}